import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.page.html',
  styleUrls: ['./detalle.page.scss'],
})
export class DetallePage implements OnInit {

  public idProyecto:string='';
  public dataProyecto:any = [];

  public portada:string="";
  public titulo:string="";
  public descripcion_full:string="";
  public interesados:string="";
  public url_facebook:string="";

  public trustedVideoUrl: SafeResourceUrl;

  constructor(private sanitizer: DomSanitizer, public http: HttpClient, public activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.idProyecto = this.activatedRoute.snapshot.paramMap.get('idProyecto');
    this.getProyectoData(this.idProyecto);
  }

  public getProyectoData(idproyecto:String){
    this.http.get('http://www.mundots.com/app_socio/app_1/lista_proyectos_crece_id.php?idProyecto='+idproyecto).subscribe(data => {
      this.dataProyecto = data;

      for(let proyecto of this.dataProyecto){
          this.portada = proyecto.portada;
          this.titulo = proyecto.titulo;
          this.descripcion_full = proyecto.descripcion_full;
          this.interesados = proyecto.interesados;
          this.trustedVideoUrl = this.sanitizer.bypassSecurityTrustResourceUrl(proyecto.url_youtube);
          this.url_facebook = proyecto.url_facebook;
      }
    })
  }

}

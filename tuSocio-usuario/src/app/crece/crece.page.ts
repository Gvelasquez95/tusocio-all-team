import { Component, OnInit } from '@angular/core';
import { AlertController, NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-crece',
  templateUrl: './crece.page.html',
  styleUrls: ['./crece.page.scss'],
})
export class CrecePage implements OnInit {

  public idSocio:string='';
  public proyectos = 'proyecto';
  public searchTerm:string = '';

  public listaAllProjects:any=[];
  public listaAllProjects2:any=[];

  public listaSelfProjects:any=[];


  constructor(public alertCtrl: AlertController, public navCtrl: NavController, public router: Router, public http: HttpClient,
    private storage: Storage) { }

  ngOnInit() {  
    this.listaAllProyectos();

    this.storage.get('idSocio').then(val => {
      this.idSocio = val;
      this.listaSelfProyectos(this.idSocio);

    });
      
  }

  public listaAllProyectos(){
    // this.http.get('http://www.mundots.com/app_socio/app_1/lista_proyectos_crece.php?all').subscribe(data => {
    //   this.listaAllProjects = data;
    //   this.listaAllProjects2 = data;
    // })
  }

  public listaSelfProyectos(idSocio:string){
    this.http.get('http://www.mundots.com/app_socio/app_1/lista_proyectos_crece.php?selfproject&socio='+idSocio).subscribe(data => {
      this.listaSelfProjects = data;

      if(this.listaSelfProjects.length == '0'){
        this.tieneProyecto();
      }

      console.log(this.listaSelfProjects.length);
    })
  }

  public goDetail(id:string){
    this.router.navigate(['/crece/detalle', {idProyecto:id}]);
  }

  async tieneProyecto(){
    const alert = await this.alertCtrl.create({
      backdropDismiss: false,
      header: "Tu Socio",
      message: "¡Hola! Bienvenido a CY CRECE, aquí podras encontrar potenciales socios que te ayudaran a impulsar tu emprendimiento. ¿Deseas registrar tu emprendimiento?",
      buttons: [
        {
          text: "Ahora no",
          role: "destructive",
          handler: () => {
            alert.dismiss();
          }
        },
        {
          text: "Ver tutorial",
          handler: () => {
            this.navCtrl.navigateForward('/crece/tutorial');
          }
        },
        {
          text: "Registrar",
          handler: () => {
            this.navCtrl.navigateForward('/crece/registro');
          }
        },
      ]
    });

    await alert.present();
  }

  public segmentChanged(ev:any){
    this.proyectos = ev.detail.value;
    console.log(ev);
  }

  searchList(): void {
    const query = (this.searchTerm && this.searchTerm !== null) ? this.searchTerm : '';
    this.listaAllProjects = this.filterList(this.listaAllProjects2 , query);
  }

  filterList(list, query): Array<any> {
    return list.filter(item => item.titulo.toLowerCase().includes(query.toLowerCase()));
  }

  addProyecto(){
    this.navCtrl.navigateForward('/menu/crece/registro')
  }

}

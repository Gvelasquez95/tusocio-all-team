import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {

  public idSocio:string="";
  public url_yt:string="";
  public url_fb:string="";

  constructor(public toastCtrl: ToastController, private storage: Storage) { }

  ngOnInit() {
    this.storage.get('idSocio').then(val => {
      this.idSocio = val;
    });
  }

  public convertToEmbedYoutubeVideo(){
    if(this.url_yt == ''){
      this.maketText("Coloque el LINK de su video en la caja de texto.");
    }
    else if(!this.url_yt.includes('https://www.youtube.com/')){
      this.maketText("El LINK que ha colocado no es de YOUTUBE");
    }
    else if(this.url_yt.includes('https://www.youtube.com/embed/')){
      this.maketText("El LINK es válido, ya puede completar los demás campos");
    }
    else if(this.url_yt.includes('https://www.youtube.com/') || this.url_yt.includes('https://www.youtu.be/')){
      const url = this.url_yt.slice(-11);
      this.url_yt = "https://www.youtube.com/embed/"+url;
    }
  }

  public validateFacebookUrl(){
    if(this.url_fb != ''){

    }else if(this.url_fb.includes('https://www.facebook.com/')){
    
    }
  }

  async maketText(mensaje){
    const toast = await this.toastCtrl.create({
      message: mensaje,
      duration: 2500,
      position: "middle",
      mode: "ios"
    });
    await toast.present();
  }

}

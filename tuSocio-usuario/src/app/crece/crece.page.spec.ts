import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrecePage } from './crece.page';

describe('CrecePage', () => {
  let component: CrecePage;
  let fixture: ComponentFixture<CrecePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrecePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrecePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

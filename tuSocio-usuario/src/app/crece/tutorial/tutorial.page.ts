import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tutorial',
  templateUrl: './tutorial.page.html',
  styleUrls: ['./tutorial.page.scss'],
})
export class TutorialPage implements OnInit {

  public listaPasos:any;

  constructor() { }

  ngOnInit() {
    this.listaPasos = [
      {
        titulo: 'Paso 1',
        image: '../../../assets/tutoriales/crece/paso1.png',
        descripcion: 'Antes de agregar tu proyecto deberas tener en cuenta que tipo de proyecto es, ya sea tecnología, ecología, etc.'
      },
      {
        titulo: 'Paso 2',
        image: '../../../assets/tutoriales/crece/paso1.png',
        descripcion: ''
      },
      {
        titulo: 'Paso 3',
        image: '../../../assets/tutoriales/crece/paso1.png',
        descripcion: ''
      },
    ]
  }
  

}

import { Component } from '@angular/core';
import { Platform, Events } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {

  constructor( private platform: Platform, private splashScreen: SplashScreen, private statusBar: StatusBar,
    private androidPermissions: AndroidPermissions, public events: Events) {
  
  }

  ngOnInit(){
    this.initializeApp();
    if(this.platform.is('android')){
      this.requestAllPermissions();
      this.statusBar.styleLightContent();
    }
  }

  requestAllPermissions()
  {
    this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA, this.androidPermissions.PERMISSION.GET_ACCOUNTS,this.androidPermissions.PERMISSION.CALL_PHONE,
       this.androidPermissions.PERMISSION.WRITE_EXTERNAL, this.androidPermissions.PERMISSION.READ_EXTERNAL, this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION,this.androidPermissions.PERMISSION.SEND_SMS]);
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
  
}

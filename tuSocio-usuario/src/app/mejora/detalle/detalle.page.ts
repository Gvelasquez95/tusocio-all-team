import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.page.html',
  styleUrls: ['./detalle.page.scss'],
})
export class DetallePage implements OnInit {

  public idPlan:string;
  public cod_plan:string;
  public plan:string;

  public dataPlan:any;
  public listaBeneficios:any;

  
  //Parametros
  public descripcion:string;
  public precio:string;

  constructor(public activatedRoute: ActivatedRoute, public http:HttpClient, public router:Router) { }

  ngOnInit() {

    this.idPlan = this.activatedRoute.snapshot.paramMap.get('idPlan');
    this.cod_plan = this.activatedRoute.snapshot.paramMap.get('cod_plan');
    this.plan = this.activatedRoute.snapshot.paramMap.get('plan');

    console.log(this.idPlan);
    
    this.getDataPlan(this.idPlan);
  }

  public getDataPlan(id:string){
    this.http.get("https://www.mundots.com/app_socio/app_1/plan_id.php?plan="+id).subscribe(data => {
      this.dataPlan = data;
      console.log(this.dataPlan);

      for(let plan of this.dataPlan){
        this.descripcion = plan.descripcion;
        this.precio = plan.precio;
        this.listaBeneficios = plan.beneficio;
        
      }
    });
  }

  public buyPlan(id:string){
    this.router.navigate(['/mejora/detalle/comprar', {idPlan:id}]);
  }

}

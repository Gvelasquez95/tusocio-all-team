import { Component, OnInit } from '@angular/core';
import { Camera } from '@ionic-native/camera/ngx';
import { ToastController, LoadingController, ActionSheetController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-comprar',
  templateUrl: './comprar.page.html',
  styleUrls: ['./comprar.page.scss'],
})
export class ComprarPage implements OnInit {

  public idSocio:string;
  public cod_plan:string;

  //Parametros
  public fotoVoucher:string;
  public producto:string; 

  constructor(private camera: Camera, public toastCtrl: ToastController, public router: Router, public transfer: FileTransfer,
    public loadingCtrl: LoadingController, public actionSheetCtrl: ActionSheetController, private storage: Storage, 
    public activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.storage.get('idSocio').then(val => {
      this.idSocio = val;
      console.log("Compra plan: idsocio - "+this.idSocio);  
    })

    this.cod_plan = this.activatedRoute.snapshot.paramMap.get('idPlan');
    console.log("Codigo plan: "+this.cod_plan);
    
    if(this.cod_plan == '1'){
      this.producto = "Paquete Básico TS";
    }else if(this.cod_plan == '2'){
      this.producto = "Paquete Premium TS";
    }

  }

  async addOferta(){

    if(this.fotoVoucher == null){
      this.makeText('Debe adjuntar la foto de su voucher de pago.');
    }else{

    var url="https://www.mundots.com/app_socio/app_1/voucher.php?pago";

    // File for Upload
    var targetPath = this.fotoVoucher;

    var options = {
      fileKey: "foto",
      chunkedMode: false,
      mimeType: "multipart/form-data",
      params : {'user': this.idSocio,
                'plan': this.cod_plan},
      HttphttpMethod: 'POST'
    };

    const fileTransfer: FileTransferObject = this.transfer.create();

    const loading = await this.loadingCtrl.create(
      {
        message: "Su petición esta siendo procesada..."
      }
    );

    await loading.present();

    // Use the FileTransfer to upload the image
    fileTransfer.upload(targetPath, url, options).then(data => {
      if(data.response == 'success'){

      loading.dismiss()
        this.makeText("Su petición de plan tendra respuesta en un lapso de 24h.");
        this.router.navigate(['/home']);
      }else{
        loading.dismiss()
        this.makeText(data.response);
      }
    }, err => {
      loading.dismiss()
      this.makeText('Ocurrio un Error mientras se registraba la compra de su plan.');
    });
  }

}

  async seleccione() {
    const actionSheet = await this.actionSheetCtrl.create({
      header: 'Elija una opción',
      buttons: [
        {
          text: 'Galeria de Fotos',
          icon: 'image',
          handler: () => {
            this.openGallery();
          }
        },
        {
          text: 'Tomar Foto',
          icon: 'camera',
          handler: () => {
            this.takePhoto();
          }
        },
        {
          text: 'Cancelar',
          role: 'destructive',
          handler:()=>{
            actionSheet.dismiss();
          }
        }
      ]
    });
    await actionSheet.present();
  }

  public takePhoto() {
    this.camera.getPicture({
      quality: 100,
      sourceType: this.camera.PictureSourceType.CAMERA,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      allowEdit:true,
      saveToPhotoAlbum: true,
    }).then((imageData) => {
      this.fotoVoucher = "data:image/jpeg;base64," + imageData;
     }, (err) => {
      console.log(err);
    });
  }

  public openGallery(){
    this.camera.getPicture({
      quality: 100,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      allowEdit:true
    }).then((imageData) => {
      this.fotoVoucher = "data:image/jpeg;base64," + imageData;
     }, (err) => {
      console.log(err);
    });
  }

  async makeText(message){
    const toast = await this.toastCtrl.create({
      message: message,
      duration: 2500
    });
    await toast.present();
  }

}

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MejoraPage } from './mejora.page';

describe('MejoraPage', () => {
  let component: MejoraPage;
  let fixture: ComponentFixture<MejoraPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MejoraPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MejoraPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

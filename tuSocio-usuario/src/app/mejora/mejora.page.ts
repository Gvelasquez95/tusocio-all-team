import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-mejora',
  templateUrl: './mejora.page.html',
  styleUrls: ['./mejora.page.scss'],
})
export class MejoraPage implements OnInit {

  public mejoras = 'servicio';
  public opciones = 'listado';

  public listaServicio:any=[];
  public listaPlan:any=[];

  public listaItems:any=[];


  constructor(public http:HttpClient, public router:Router, public toastCtrl:ToastController) { }

  ngOnInit() {
    this.getListaServicios();
    this.getListaOfertas();

    this.listaItems = [
      {
        plan: '1',
        descripcion: '1 Mailling mensual en el boletín Tu Socio'
      },
      {
        plan: '1',
        descripcion: 'Sistema Contable GRATIS'
      },
      {
        plan: '1',
        descripcion: '50% de dscto. en Identidad Corporativa y Marketing digital'
      },
      {
        plan: '1',
        descripcion: '50% de dscto. en el Boletín físico'
      },
      {
        plan: '1',
        descripcion: 'Cy Crece limitado'
      },
      {
        plan: '1',
        descripcion: 'Cy Cotiza'
      },
      {
        plan: '2',
        descripcion: '1 Mailling mensual en el boletín Tu Socio'
      },
      {
        plan: '2',
        descripcion: '1 Mailling semanal exclusivo de la marca'
      },
      {
        plan: '2',
        descripcion: 'Sistema Contable GRATIS'
      },
      {
        plan: '2',
        descripcion: '50% de dscto. en Identidad Corporativa y Marketing digital'
      },
      {
        plan: '2',
        descripcion: '50% de dscto. en el Boletín físico'
      },
      {
        plan: '2',
        descripcion: 'Capacitación mensual en estrategia de ventas para 3 personas'
      },
      {
        plan: '2',
        descripcion: 'Cy Crece ilimitado'
      },
      {
        plan: '2',
        descripcion: 'Cy Cotiza'
      },
      {
        plan: '3',
        descripcion: '1 Mailling mensual en el boletín Tu Socio'
      },
      {
        plan: '3',
        descripcion: '1 Mailling semanal exclusivo de la marca'
      },
      {
        plan: '3',
        descripcion: 'Sistema Contable GRATIS'
      },
      {
        plan: '3',
        descripcion: '50% de dscto. en Identidad Corporativa y Marketing digital'
      },
      {
        plan: '3',
        descripcion: '50% de dscto. en el Boletín físico'
      },
      {
        plan: '3',
        descripcion: 'Capacitación mensual en estrategia de ventas para 3 personas'
      },
      {
        plan: '3',
        descripcion: 'Cy Crece ilimitado'
      },
      {
        plan: '3',
        descripcion: 'Cy Cotiza'
      },
      {
        plan: '3',
        descripcion: 'Delivery Chassky Ordenes de pedidos y entregas'
      }

    ]

  }

    public getListaServicios(){
      this.http.get("http://www.mundots.com/app_socio/app_1/lista_servicio.php").subscribe(data => {
        this.listaServicio = data;
      })
    }

    public getListaOfertas(){
    //   this.http.get("https://cyhomework.com/app_socio/app_1/plan.php").subscribe(data => {
    //     this.listaPlan = data;
    //     console.log(this.listaPlan);
    //   });

      this.listaPlan = [
        {
          idPlan:'2',
          plan:'1',
          descripcion:'BÁSICO',
          foto:'../../assets/images/basico.jpg',
        },
        {
          idPlan:'3',
          plan:'2',
          descripcion:'PREMIUM',
          foto:'../../assets/images/premium.jpg',
        },
        {
          idPlan:'4',
          plan:'3',
          descripcion:'VIP',
          foto:'../../assets/images/vip.jpg',
        }
      ]
      console.log(this.listaPlan);
    }

  public irDetalle(idplan:string,cod:string,descripcion:string){
    if(idplan == '4'){
      this.makeText("¡Proximamente plan para multicadenas!")
    }else{
    this.router.navigate(['/mejora/detalle', {idPlan:idplan,cod_plan:cod, plan:descripcion}]);
    }
  }

  async makeText(mensaje:string){
    const toast = await this.toastCtrl.create(
      {
        message: mensaje,
        duration: 2500
      }
    );
    await toast.present();
  }

  public segmentChanged(ev:any){
    this.mejoras = ev.detail.value;
    console.log(ev);
  }

  public segmentChanged2(ev:any){
    this.opciones = ev.detail.value;
    console.log(ev);
  }

}

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroFase2Page } from './registro-fase2.page';

describe('RegistroFase2Page', () => {
  let component: RegistroFase2Page;
  let fixture: ComponentFixture<RegistroFase2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistroFase2Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroFase2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

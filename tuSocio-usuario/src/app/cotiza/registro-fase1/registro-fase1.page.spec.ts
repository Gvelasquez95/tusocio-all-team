import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroFase1Page } from './registro-fase1.page';

describe('RegistroFase1Page', () => {
  let component: RegistroFase1Page;
  let fixture: ComponentFixture<RegistroFase1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistroFase1Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroFase1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

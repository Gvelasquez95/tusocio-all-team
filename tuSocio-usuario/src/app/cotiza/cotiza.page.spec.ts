import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CotizaPage } from './cotiza.page';

describe('CotizaPage', () => {
  let component: CotizaPage;
  let fixture: ComponentFixture<CotizaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CotizaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CotizaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

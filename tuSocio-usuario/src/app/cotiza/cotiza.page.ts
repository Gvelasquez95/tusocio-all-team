import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-cotiza',
  templateUrl: './cotiza.page.html',
  styleUrls: ['./cotiza.page.scss'],
})
export class CotizaPage implements OnInit {

  public opciones = 'peticion';
  public searchTerm:string = '';
  public listaRecibidos:any = [];
  public listaEnviados:any = [];

  public listaRecibidos2:any = [];
  public listaEnviados2:any = [];

  public listaCategorias:any = [];

  constructor(public router: Router, public http: HttpClient, public menu: MenuController) { 
    this.menu.enable(false, 'menuTS');
  }

  ngOnInit() { 

    this.listaEnviados = [
      {
        foto: "",
        empresa: "Juanito",
        descripcion: "Hola",
        fecha: "10 mayo"
      },
      {
        foto: "",
        empresa: "Pepito",
        descripcion: "Negro",
        fecha: "10 mayo"
      },
      {
        foto: "",
        empresa: "Jorgito",
        descripcion: "Como estas",
        fecha: "9 mayo"
      },
    ]

    this.listaRecibidos = [
      {
        foto: "",
        empresa: "Luisa",
        descripcion: "habla",
        fecha: "8 mayo"
      },
      {
        foto: "",
        empresa: "Fiorella",
        descripcion: "pues",
        fecha: "2 mayo"
      },
      {
        foto: "",
        empresa: "Kelly",
        descripcion: "nero",
        fecha: "15 abril"
      },
    ]

    this.listaEnviados2 = [
      {
        foto: "",
        empresa: "Juanito",
        descripcion: "Hola",
        fecha: "10 mayo"
      },
      {
        foto: "",
        empresa: "Pepito",
        descripcion: "Negro",
        fecha: "10 mayo"
      },
      {
        foto: "",
        empresa: "Jorgito",
        descripcion: "Como estas",
        fecha: "9 mayo"
      },
    ]

    this.listaRecibidos2 = [
      {
        foto: "",
        empresa: "Luisa",
        descripcion: "habla",
        fecha: "8 mayo"
      },
      {
        foto: "",
        empresa: "Fiorella",
        descripcion: "pues",
        fecha: "2 mayo"
      },
      {
        foto: "",
        empresa: "Kelly",
        descripcion: "nero",
        fecha: "15 abril"
      },
    ]

    this.listaCategorias = [
      {
        valor: '1',
        descripcion: 'Todos'
      },
      {
        valor: '2',
        descripcion: 'Decoración'
      },
      {
        valor: '3',
        descripcion: 'Comida'
      },
      {
        valor: '4',
        descripcion: 'Moda'
      },
      {
        valor: '5',
        descripcion: 'Utiles de Oficina'
      },
      {
        valor: '6',
        descripcion: 'Tecnologia'
      },
      {
        valor: '7',
        descripcion: 'Transporte'
      },
      {
        valor: '8',
        descripcion: 'Artefactos electricos'
      }

    ]

  }

  public segmentChanged(ev:any){
    this.opciones = ev.detail.value;
    console.log(ev);
  }

  searchList(): void {
    const query = (this.searchTerm && this.searchTerm !== null) ? this.searchTerm : '';

    if (this.opciones == 'cotizacion') {
      this.listaRecibidos = this.filterList(this.listaRecibidos2 , query);
    }else if (this.opciones == 'mi_cotizacion') {
      this.listaEnviados = this.filterList(this.listaEnviados2 , query);
    }
    
  }

  filterList(list, query): Array<any> {
    return list.filter(item => item.empresa.toLowerCase().includes(query.toLowerCase()));
  }

  public goMail(){
    this.router.navigate(['/menu/cotiza/detalle']);
  }

  public listarCategoria(idCategoria){
    this.http.get('')
  }

  public goPage(pagina){
    this.router.navigate([pagina]);
  }

}

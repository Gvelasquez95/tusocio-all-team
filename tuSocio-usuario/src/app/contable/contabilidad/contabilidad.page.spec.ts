import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContabilidadPage } from './contabilidad.page';

describe('ContabilidadPage', () => {
  let component: ContabilidadPage;
  let fixture: ComponentFixture<ContabilidadPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContabilidadPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContabilidadPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

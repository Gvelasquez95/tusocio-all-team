import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContablePage } from './contable.page';

describe('ContablePage', () => {
  let component: ContablePage;
  let fixture: ComponentFixture<ContablePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContablePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContablePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner/ngx';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {

  options: BarcodeScannerOptions;

  encodeText:string="";
  encodeData:any = {};

  public qrcodeScanned:any=[];

  constructor(private barcodeScanner: BarcodeScanner, public toastCtrl: ToastController) { 

  }

  ngOnInit() {
    
  }
  
  public scan(){
    this.options = {
      prompt: "Escanea el codigo QR de tu proveedor"
    };

    this.barcodeScanner.scan(this.options).then((data) => {
      this.qrcodeScanned = data;
    }, (err) => {
      console.log("Error: " + err);
    }
    )
  }

  public encode(){
    this.barcodeScanner.encode(this.barcodeScanner.Encode.TEXT_TYPE, this.encodeText).then((data) => {
      this.encodeData = data;
    },
    (err) => { 
      console.log('Error: '+err);
    })
  }

  async makeText(mensaje:string){
    const toast = await this.toastCtrl.create({
      message: mensaje,
      duration: 2000
    });

    await toast.present();
  }

}

import { Component, OnInit } from '@angular/core';
import { BarcodeScannerOptions, BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { ToastController } from '@ionic/angular';
import { encode } from 'punycode';

@Component({
  selector: 'app-proveedores',
  templateUrl: './proveedores.page.html',
  styleUrls: ['./proveedores.page.scss'],
})
export class ProveedoresPage implements OnInit {

  option = 'proveedores';

  options: BarcodeScannerOptions;

  public qrcodeScanned:any=[];

  public ListaProveedores:any;

  public qrCode = [{ name: 'John', email: 'john@awesome.com' }]
  public createdCode = null;

  constructor(private barcodeScanner: BarcodeScanner, public toastCtrl: ToastController) { }

  ngOnInit() {
    this.encode();
    this.ListaProveedores =[
      {
        idProveedor: '1',
        foto: '',
        nombre: 'Vladimir Paz',
        ruc: '123456789'
      },
      {
        idProveedor: '2',
        foto: '',
        nombre: 'Kevin Moreno',
        ruc: '234567890'
      }
    ]
  }

  public segmentChanged(ev:any){
    this.option = ev.detail.value;
    console.log(ev);
  }

  public scan(){
    this.options = {
      prompt: "Escanea el codigo QR de tu proveedor",
      showTorchButton: true,
      showFlipCameraButton: true,
      disableSuccessBeep: false
  };
    this.barcodeScanner.scan(this.options).then((data) => {
      this.qrcodeScanned = data;
    }, (err) => {
      console.log("Error: " + err);
    }
    )
  }

  public encode(){
    this.createdCode = this.qrCode;
  }

  async makeText(mensaje:string){
    const toast = await this.toastCtrl.create({
      message: mensaje,
      duration: 2000
    });

    await toast.present();
  }

}

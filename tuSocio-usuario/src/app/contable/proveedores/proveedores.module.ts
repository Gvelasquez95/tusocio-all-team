import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ProveedoresPage } from './proveedores.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { NgxQRCodeModule } from 'ngx-qrcode3';

const routes: Routes = [
  {
    path: '',
    component: ProveedoresPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    NgxQRCodeModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ProveedoresPage]
})
export class ProveedoresPageModule {}

import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController, ToastController, IonSearchbar, NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-contable',
  templateUrl: './contable.page.html',
  styleUrls: ['./contable.page.scss'],
})
export class ContablePage implements OnInit {

  public listaOpciones:any;
  public confirmPassAlert:any;

  public idSocio:string;
  public empresaData:any = [];
  public tipo:string="";
  public confirmaclave:string;

  constructor(public alertCtrl: AlertController, private storage: Storage, public loadingCtrl: LoadingController, private http: HttpClient,
    public toastCtrl: ToastController, public navCtrl: NavController) { }

  ngOnInit() {
    this.storage.get("validaTSContable").then(data => {
      if(data != '1'){
        this.storage.get('idSocio').then(data => {
          this.idSocio = data;
          this.getDataEmpresa(this.idSocio);
        })
        this.confirmPassAlertBox();
      }
  })
  
    this.listaOpciones = [
      {
        image: '../../assets/images/img1.png',
        titulo: 'Ingresos y Egresos',
        pagina: '/menu/contable/contabilidad'
      },
      {
        image: '../../assets/images/img2.png',
        titulo: 'Proveedores',
        pagina: '/menu/contable/proveedores'
      },
      {
        image: '../../assets/images/img3.png',
        titulo: 'Clientes',
        pagina: '/menu/contable/contabilidad'
      },
      {
        image: '../../assets/images/img4.png',
        titulo: 'Ventas',
        pagina: '/menu/contable/ventas'
      },
      {
        image: '../../assets/images/img5.png',
        titulo: 'Productos y Servicios',
        pagina: '/menu/contable/inventario'
      },
      {
        image: '../../assets/images/img6.png',
        titulo: 'Colaboradores',
        pagina: '/menu/contable/colaboradores'
      },
    ]
  }

  public getDataEmpresa(id:string){
    this.http.get("https://mundots.com/app_socio/app_1/lista_user.php?empresa&user="+id).subscribe(data => {
      this.empresaData = data;
      for(let empresa of this.empresaData){
        this.tipo = empresa.tipo_negocio;
        this.storage.set('tipo_negocio',this.tipo);
        console.log("tipo_negocio: ", empresa.tipo_negocio);
      }
    });
  }

  async confirmPassAlertBox(){
    this.confirmPassAlert = await this.alertCtrl.create ({
        header:"Confirmar Contraseña",
        backdropDismiss: false,
        animated: true,
        mode: "ios",
        translucent: true,
        inputs: [
          {
            name: 'cnclave',
            type: 'password',
            placeholder: 'Ingrese su contraseña'
          }
        ],
        buttons: [
          {
            text: 'Salir',
            role: 'destructive',
            handler: () => {
              this.navCtrl.navigateBack('/menu/home');
              this.confirmPassAlert.dismiss();
              return false;
            }          
          },{
            text: 'Continuar',
            handler: data => {
              this.confirmaclave = data.cnclave;
              this.confirmPass();
              return false;
            }          
          }
        ]   
      });

      await this.confirmPassAlert.present();
  }

  async confirmPass(){

    console.log(this.confirmaclave);
    

    var url="https://mundots.com/app_socio/app_1/conf_pass.php?negocio";

    var fp = new FormData();
    fp.append('user', this.idSocio);
    fp.append('clave', this.confirmaclave);

    const loading = await this.loadingCtrl.create(
      {
        message: "Verificando Datos...",
        duration: 1500
      }
    );
    await loading.present();

          this.http.post(url, fp).subscribe(data => {
          let result = JSON.parse(JSON.stringify(data || null ));
          let mensaje=result.status;

            if(mensaje == "success"){
              this.makeText("Tus datos han sido verificados, Bienvenido a Tu Socio Contable");
              this.confirmPassAlert.dismiss();
              this.storage.set("validaTSContable","1");
            }
            else{
              this.makeText(mensaje);
            }
              this.loadingCtrl.dismiss();
            },err => {
              this.loadingCtrl.dismiss();
            console.log(err);
            });
  }

  async makeText(mensaje:string){
    const toast = await this.toastCtrl.create({
      message: mensaje,
      duration: 2500
    });

    await toast.present();
  }

  public goPage(pagina){
    this.navCtrl.navigateForward(pagina);
  }

}

import { Component, OnInit } from '@angular/core';
import { ToastController, LoadingController, Events, NavController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-servicio',
  templateUrl: './servicio.page.html',
  styleUrls: ['./servicio.page.scss'],
})
export class ServicioPage implements OnInit {

  public tipo_negocio:string="";
  public idNegocio:string="";
  //Parametros Servicio
  public fotoServicio:string="";
  public titulo:string="";
  public precio:string="";
  public descripcion:string="";

  constructor(public loadingCtrl: LoadingController, public toastCtrl: ToastController, public http: HttpClient, public events: Events,
    public navCtrl: NavController, private transfer: FileTransfer, private storage: Storage) { }

  ngOnInit() {
    this.storage.get('idNegocio').then(data => {
      this.idNegocio = data;
    });
    this.storage.get('tipo_negocio').then(data => {
      this.tipo_negocio = data;
    });
  }

  async addProducto(){

    if(this.fotoServicio == ""){
            var url="https://www.mundots.com/app_socio/app_1/conta/conta2.php?registra_serv";

            var fp = new FormData();
            fp.append('tienda', this.idNegocio);
            fp.append('titulo', this.titulo);
            fp.append('precio', this.precio);
            fp.append('descripcion', this.descripcion);
            fp.append('tipo', this.tipo_negocio);

            const loading = await this.loadingCtrl.create(
              {
                message: "Estamos registrando su servicio..."
              }
            );

            await loading.present();
        
            this.http.post(url, fp).subscribe(data => {
            let result = JSON.parse(JSON.stringify(data || null ));
            let mensaje=result.status;
        
            if(mensaje == "success"){
              
              this.events.publish('register:servicio');
              this.makeText("!Bien¡ tu servicio ha sido registrado.");
              loading.dismiss();
              this.navCtrl.navigateRoot('/menu/contable/inventario');
            }
            else{
              this.makeText(mensaje);
            }
              this.loadingCtrl.dismiss();
            },err => {

              this.loadingCtrl.dismiss();
              this.makeText('Ocurrio un error mientras se registraba su servicio, intentelo nuevamente.');
              });
    }else{
          var url="https://www.mundots.com/app_socio/app_1/conta/conta2.php?registra_serv";

          // File for Upload
          var targetPath = this.fotoServicio;

          var options = {
            fileKey: "foto",
            chunkedMode: false,
            mimeType: "multipart/form-data",
            params : {'tienda': this.idNegocio,
                      'titulo': this.titulo,
                      'precio': this.precio,
                      'descripcion': this.descripcion,
                      'tipo': this.tipo_negocio},
            HttphttpMethod: 'POST'
          };

          const fileTransfer: FileTransferObject = this.transfer.create();

          const loading = await this.loadingCtrl.create(
            {
              message: "Estamos registrando su servicio..."
            }
          );

          await loading.present();

          // Use the FileTransfer to upload the image
          fileTransfer.upload(targetPath, url, options).then(data => {
            if(data.response == 'success'){

              this.events.publish('register:servicio');
              this.makeText("!Bien¡ tu servicio ha sido registrado.");
              loading.dismiss();
              this.navCtrl.navigateRoot('/menu/contable/inventario');

            }else{
              loading.dismiss()
              this.makeText(data.response);
            }
          }, err => {
            loading.dismiss()
            this.makeText('Ocurrio un error mientras se registraba su servicio, intentelo nuevamente.');
          });
     }
  }

  async makeText(mensaje:string){
    const toast = await this.toastCtrl.create({
      message: mensaje,
      duration: 2500,
    });

    await toast.present();
  }

}

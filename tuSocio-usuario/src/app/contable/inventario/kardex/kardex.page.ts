import { Component, OnInit } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-kardex',
  templateUrl: './kardex.page.html',
  styleUrls: ['./kardex.page.scss'],
})
export class KardexPage implements OnInit {

  public kardex='entrada';

  constructor(public actionSheetCtrl: ActionSheetController) { }

  ngOnInit() {
  }

  public segmentChanged(ev:any){
    this.kardex = ev.detail.value;
    console.log(ev);
  }

  async showOptions(){
    const sheet = await this.actionSheetCtrl.create({
      backdropDismiss: true,
      buttons: [
        {
          text: 'Convertir a PDF',
          icon: '../../../assets/icon/pdf.svg',
          handler: () => {

          }
        },
        {
          text: 'Convertir a XLS',
          icon: '../../../assets/icon/xls.svg',
          handler: () => {

          }
        },
        {
          text: 'Convertir a CSV',
          icon: '../../../assets/icon/csv.svg',
          handler: () => {

          }
        },
        {
          text: 'Cerrar',
          icon: 'close',
          role: 'destructive',
          handler: () => {
            this.actionSheetCtrl.dismiss(); 
          }
        }
      ]
    }
    );

    await sheet.present();
  }

}

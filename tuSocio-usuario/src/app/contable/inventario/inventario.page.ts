import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NegocioDataComponent } from 'src/app/negocio-data/negocio-data.component';
import { PopoverController, Events, ActionSheetController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-inventario',
  templateUrl: './inventario.page.html',
  styleUrls: ['./inventario.page.scss'],
})
export class InventarioPage implements OnInit {

  public idSocio:string;
  public listaProductos:any;

  public empresaData:any = [];
  public tipo:string;

  constructor(public router: Router, public popoverCtrl: PopoverController, public http: HttpClient, public storage: Storage,
    public events:Events, public actionSheetCtrl: ActionSheetController) { }

  ngOnInit() {
    this.storage.get('idSocio').then(data => {
      this.idSocio = data;
      this.getListaProductos(this.idSocio);
      this.getDataEmpresa(this.idSocio);
    })
  }

  public getDataEmpresa(id:string){
    this.http.get("https://mundots.com/app_socio/app_1/lista_user.php?empresa&user="+id).subscribe(data => {
      this.empresaData = data;
      for(let empresa of this.empresaData){
        this.tipo = empresa.tipo_negocio;
        this.storage.set('tipo_negocio',this.tipo);
        console.log("tipo_negocio: ", empresa.tipo_negocio);
      }
    });
  }

  ionViewWillEnter(){
    this.events.subscribe('register:producto', () => {
      this.getListaProductos(this.idSocio);
    })
  }

  public getListaProductos(id:string){
    this.http.get("https://mundots.com/app_socio/app_1/conta/productos.php?list&tienda="+id).subscribe(data => {
      this.listaProductos = data;
    })
  }

  public goKardex(){
    this.router.navigate(['/menu/contable/inventario/kardex']);
  }

  public irDetalle(id:string){
    this.router.navigate(['/menu/contable/inventario/detalle', {idProducto: id} ]);
  }

  async negocioDetalle(ev: any) {
    const popover = await this.popoverCtrl.create({
      component: NegocioDataComponent,
      event: ev,
      translucent: true
    });
    return await popover.present();
  }

  public goPage(pagina:string, idsocio:string){
    this.router.navigate([pagina, {idSocio:idsocio}]);
  }

}

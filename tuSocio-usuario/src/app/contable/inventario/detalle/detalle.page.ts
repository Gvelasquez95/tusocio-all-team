import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.page.html',
  styleUrls: ['./detalle.page.scss'],
})
export class DetallePage implements OnInit {

  public idProducto="";
  public idNegocio="";
  public dataProducto;
  //Parametros Producto
  public fotoProducto:string="";
  public nombre:string="";
  public marca:string="";
  public stock:string="";
  public uni_med:string="";
  public precio_c:number;
  public precio_v:number;
  public observaciones:string="";
  public codigoBarra:any;

  constructor(public http: HttpClient, public activatedRoute: ActivatedRoute, public barcodeScanner: BarcodeScanner, private storage: Storage) { }

  ngOnInit() {
    this.idProducto = this.activatedRoute.snapshot.paramMap.get('idProducto');
    this.storage.get('idNegocio').then(data => {
      this.idNegocio = data;
      this.getDataProducto(this.idProducto,this.idNegocio);
    });
  }

  public getDataProducto(producto:string,tienda:string){
    this.http.get("https://www.mundots.com/app_socio/app_1/conta/lista_producto_id.php?id="+producto+"&tienda="+tienda).subscribe(data => {
      this.dataProducto = data;
      for (let p of this.dataProducto){
        this.fotoProducto = p.foto;
        this.nombre = p.nombre;
        this.marca = p.marca;
        this.stock = p.stock;
        this.uni_med = p.uni_med;
        this.precio_c = p.precio_c;
        this.precio_v = p.precio_v;
        this.observaciones = p.observaciones;
        this.codigoBarra = p.codigo;
      }
    })
  }

  
  public getbarCode(){
    this.barcodeScanner.scan().then(barcodeData => {
        this.codigoBarra = barcodeData.text;
     }).catch(err => {
         console.log('Error', err);
     });
  }

}

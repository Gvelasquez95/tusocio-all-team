import { Component, OnInit } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { ToastController, LoadingController, NavController, Events } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {

  public idNegocio:string="";
  public tipo_negocio:string="";
  //Parametros Producto
  public fotoProducto:string="";
  public nombre:string="";
  public marca:string="";
  public stock:string="";
  public uni_med:string="";
  public precio_c:number;
  public precio_v:number;
  public observaciones:string="";
  public codigoBarra:any;

  constructor(private barcodeScanner: BarcodeScanner, public toastCtrl: ToastController, public transfer: FileTransfer,
    public loadingCtrl: LoadingController, public http: HttpClient, public navCtrl: NavController, public events: Events,
    private storage: Storage) { }

  ngOnInit() {
    this.storage.get('idNegocio').then(data => {
      this.idNegocio = data;
    });
    this.storage.get('tipo_negocio').then(data => {
      this.tipo_negocio = data;
    });
  }

  public getbarCode(){
    this.barcodeScanner.scan().then(barcodeData => {
        this.codigoBarra = barcodeData.text;
     }).catch(err => {
         console.log('Error', err);
     });
  }

  async addProducto(){

    if(this.fotoProducto == ""){
        if(this.precio_v > this.precio_c){
            var url="https://www.mundots.com/app_socio/app_1/conta/conta2.php?registro_pro";

            var fp = new FormData();
            fp.append('tienda', this.idNegocio);
            fp.append('nombre', this.nombre);
            fp.append('marca', this.marca);
            fp.append('codigo', this.codigoBarra);
            fp.append('precio_c', ""+this.precio_c);
            fp.append('precio_v', ""+this.precio_v);
            fp.append('observacion', this.observaciones);
            fp.append('cantidad', this.stock);
            fp.append('uni_med', this.uni_med);
            fp.append('tipo', this.tipo_negocio);

            const loading = await this.loadingCtrl.create(
              {
                message: "Estamos registrando su producto..."
              }
            );

            await loading.present();
        
            this.http.post(url, fp).subscribe(data => {
            let result = JSON.parse(JSON.stringify(data || null ));
            let mensaje=result.status;
        
            if(mensaje == "success"){
              
              this.events.publish('register:producto');
              this.makeText("!Bien¡ tu producto ha sido registrado.");
              loading.dismiss();
              this.navCtrl.navigateRoot('/menu/contable/inventario');
            }
            else{
              this.makeText(mensaje);
            }
              this.loadingCtrl.dismiss();
            },err => {

              this.loadingCtrl.dismiss();
              this.makeText('Ocurrio un Error mientras se actuliazaba su información, intentelo nuevamente.');
              });
          }else{
            this.makeText("El precio de venta debe ser mayor al precio de compra del producto.")
          }
        }else{

          var url="https://www.mundots.com/app_socio/app_1/conta/conta.php?registro_pro";

          // File for Upload
          var targetPath = this.fotoProducto;

          var options = {
            fileKey: "foto",
            chunkedMode: false,
            mimeType: "multipart/form-data",
            params : {'tienda': this.idNegocio,
                      'nombre': this.nombre,
                      'marca': this.marca,
                      'codigo': this.codigoBarra,
                      'precio_c': this.precio_c,
                      'precio_v': this.precio_v,
                      'observacion': this.observaciones,
                      'cantidad': this.stock,
                      'uni_med': this.uni_med,
                      'tipo': this.tipo_negocio},
            HttphttpMethod: 'POST'
          };

          const fileTransfer: FileTransferObject = this.transfer.create();

          const loading = await this.loadingCtrl.create(
            {
              message: "Estamos registrando su producto..."
            }
          );

          await loading.present();

          // Use the FileTransfer to upload the image
          fileTransfer.upload(targetPath, url, options).then(data => {
            if(data.response == 'success'){

              this.events.publish('register:producto');
              this.makeText("!Bien¡ tu producto ha sido registrado.");
              loading.dismiss();
              this.navCtrl.navigateRoot('/menu/contable/inventario');

            }else{
              loading.dismiss()
              this.makeText(data.response);
            }
          }, err => {
            loading.dismiss()
            this.makeText('Ocurrio un error mientras se registraba su producto, intentelo nuevamente.');
          });
        }
  }

  async makeText(mensaje:string){
    const toast = await this.toastCtrl.create({
      message: mensaje,
      duration: 2500,
    });

    await toast.present();
  }

}

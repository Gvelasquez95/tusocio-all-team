import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastController, LoadingController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.page.html',
  styleUrls: ['./carrito.page.scss'],
})
export class CarritoPage implements OnInit {

  public idNegocio:string="";
  public idUsuario:string="";

  public code:string;

  public ruc:string="";
  public cliente:string="";

  public existeCliente;

  public listaCarrito:any;
  listaCarrito2:any;
  public monto_total:number=0.0;

  constructor(public activatedRoute: ActivatedRoute, public toastCtrl: ToastController, public http: HttpClient,
    public loadingCtrl: LoadingController) { 
    this.existeCliente = "1";
  }

  ngOnInit() {
    this.code = this.activatedRoute.snapshot.paramMap.get('codigo');
    this.idNegocio = this.activatedRoute.snapshot.paramMap.get('idNegocio');
    this.idUsuario = this.activatedRoute.snapshot.paramMap.get('idUsuario');

    this.getListaCarrito(this.idNegocio,this.idUsuario);
  }

  public getListaCarrito(negocio:string,usuario:string){
    this.http.get('https://www.mundots.com/app_socio/app_1/conta/conta2.php?list_carrito&tienda='+negocio+'&user='+usuario).subscribe(data => {
      this.listaCarrito = data;
      this.monto_total=0.0;
     for (let i = 0; i < this.listaCarrito.length; i++) {
      var precio=this.listaCarrito[i].precio;
      var cantidad=this.listaCarrito[i].cantidad;
      this.listaCarrito[i]['total']=precio*cantidad;
      this.monto_total+=this.listaCarrito[i]['total'];
     }
    })
  }

  async removeItem(idProducto:string){
    var url="https://www.mundots.com/app_socio/app_1/conta/conta2.php?delete_pro_carrito";

    var fc = new FormData();
    fc.append('tienda', this.idNegocio);
    fc.append('producto', idProducto);
    fc.append('user', this.idUsuario);

    const loading = await this.loadingCtrl.create(
      {
        message: "Eliminando el producto de su lista de compra..."
      }
    );

    await loading.present();

    this.http.post(url, fc).subscribe(data => {
      this.getListaCarrito(this.idNegocio,this.idUsuario);
      this.makeText('Producto eliminado.');
      this.loadingCtrl.dismiss();
    },err => {
      this.loadingCtrl.dismiss();
      this.makeText('Ocurrio un Error mientras se eliminaba el producto de su carrito.');
   });
  }

  public validarCliente(){
    this.existeCliente = "0";
  }

  async makeText(mensaje){
    const toast = await this.toastCtrl.create(
      {
        message: mensaje,
        duration: 2500
      }
    );
    await toast.present();      
  }

}

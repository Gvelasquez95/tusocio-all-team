import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { ToastController, LoadingController, NavController, AlertController, Events } from '@ionic/angular';

@Component({
  selector: 'app-ventas',
  templateUrl: './ventas.page.html',
  styleUrls: ['./ventas.page.scss'],
})
export class VentasPage implements OnInit {

  public idSocio:string;
  public idNegocio:string;
  public tipo_negocio:string="";

  public ListaProductos:any;

  constructor(public router: Router, public http: HttpClient, private storage: Storage, public toastCtrl: ToastController,
    public loadingCtrl: LoadingController, public navCtrl: NavController, public alertCtrl: AlertController, public events: Events) { }

  ngOnInit() {
    this.storage.get('idSocio').then(data => {
      this.idSocio = data;
      console.log("idsocio ventas: "+this.idSocio);
    })

    this.storage.get('idNegocio').then(data => {
      this.idNegocio = data;
      console.log("idnegocio ventas: "+this.idNegocio);
      this.getListaProductos(this.idNegocio);
    })

    this.storage.get('tipo_negocio').then(data => {
      this.tipo_negocio = data;
      console.log("tipo negocio: "+this.tipo_negocio);
    })
  }

  public goPage(pagina,code){
    this.router.navigate([pagina, {codigo:code}]);
  }

  public getListaProductos(id:String){
    this.http.get('https://www.mundots.com/app_socio/app_1/conta/productos.php?list&tienda='+id).subscribe(data => {
      this.ListaProductos = data;
    })
  }

  async chooseQuantity(idProducto:string, stock:number){
    const stockAlert = await this.alertCtrl.create(
      {
        header: 'Elije cantidad',
        message: 'STOCK DISPONIBLE : '+stock,
        mode: 'ios',
        inputs: [
          {
            name: 'cantidad',
            type: 'number',
            placeholder: 'Ingrese cantidad'
          }
        ],
        buttons: [
          {
            text: 'Listo',
            handler: data => {
              console.log(data.cantidad);
              console.log(stock);
              stock=parseInt(""+stock); 
              if(data.cantidad > stock){

                console.log(data.cantidad);
                console.log(stock);
                
                
                this.makeText('La cantidad ingresada, supera el stock máximo del producto');
              }else{
                this.addCarrito(idProducto,data.cantidad);
              }
            }          
          }]   
      });
    await stockAlert.present();
  }

  async addCarrito(idProducto:string, cantidad:string){

    var url="https://www.mundots.com/app_socio/app_1/conta/conta2.php?add_carrito";

    var fc = new FormData();
    fc.append('tienda', this.idNegocio);
    fc.append('producto', idProducto);
    fc.append('cantidad', cantidad);
    fc.append('user', this.idSocio);
    fc.append('tipo', this.tipo_negocio);

    const loading = await this.loadingCtrl.create(
      {
        message: "Estamos agregando el producto a su carrito..."
      }
    );

    await loading.present();

    this.http.post(url, fc).subscribe(data => {
      this.getListaProductos(this.idNegocio);
      this.makeText('Producto agregado al carrito.');
      this.loadingCtrl.dismiss();
    },err => {
      this.loadingCtrl.dismiss();
      this.makeText('Ocurrio un Error mientras se agregaba el producto a su carrito.');
   });
  }

  async gotoPay(){
    const alert = await this.alertCtrl.create({
      backdropDismiss: true,
      header: 'Comprobante de Pago',
      message: 'Seleccione un tipo de comprobante de pago.',
      buttons: [
        {
          text: 'Factura',
          handler: () => {
            this.router.navigate(['/menu/contable/ventas/carrito', {codigo:'2',idNegocio: this.idNegocio, idUsuario: this.idSocio}]);
          }
        },
        {
          text: 'Boleta',
          handler: () => {
            this.router.navigate(['/menu/contable/ventas/carrito', {codigo:'1',idNegocio: this.idNegocio, idUsuario: this.idSocio}]);
          }
        }
    ]
    });

    await alert.present();
  }

  async makeText(mensaje){
    const toast = await this.toastCtrl.create({
      message: mensaje,
      duration: 2500
    });

    await toast.present();
  }

}

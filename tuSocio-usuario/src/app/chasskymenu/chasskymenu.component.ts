import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-chasskymenu',
  templateUrl: './chasskymenu.component.html',
  styleUrls: ['./chasskymenu.component.scss'],
})
export class ChasskymenuComponent implements OnInit {

  public listaOpciones:any = []

  constructor(public router:Router, public popoverCtrl: PopoverController) { }

  ngOnInit() {
    this.listaOpciones = [
      {
        text: 'Metodos de pago',
        url: '',
        icon: 'card'
      },
      {
        text: 'Información de la factura',
        url: '',
        icon: 'document'
      },
      {
        text: 'Código promocional',
        url: '',
        icon: 'pricetag'
      },
      {
        text: 'Preguntas',
        url: '',
        icon: 'help-circle-outline'
      },
      {
        text: 'Contacto',
        url: '',
        icon: 'mail'
      },
      {
        text: 'Salir',
        url: '/home',
        icon: 'log-out'
      },
    ]
  }

  public goOption(pagina){
    this.router.navigate([pagina]);
    this.popoverCtrl.dismiss();
  }

}

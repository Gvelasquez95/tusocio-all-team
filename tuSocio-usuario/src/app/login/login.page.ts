import { Component, OnInit, ViewChild } from '@angular/core';
import { MenuController, ModalController } from '@ionic/angular';
import { LoginModalPage } from '../login-modal/login-modal.page';
import { SignupModalPage } from '../signup-modal/signup-modal.page';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
 
  constructor(public menu: MenuController, public modalCtrl: ModalController) {
    this.menu.enable(false, 'menuTS');
  }

  ionViewWillEnter(){
    var videoBg = <HTMLMediaElement>document.getElementById('videoBg');
    videoBg.play();
  }

  ionViewWillLeave(){
    var videoBg = <HTMLMediaElement>document.getElementById('videoBg');
    videoBg.pause();
  }

  ngOnInit() {
    
  }

  async callLogin(){
    const modal = await this.modalCtrl.create({
      component: LoginModalPage,
      mode: 'ios',
      cssClass: 'loginModal animated fadeInUp',
    });
    
    return await modal.present();
  }

  async callSignUp(){
    const modal = await this.modalCtrl.create({
      component: SignupModalPage,
      mode: 'ios',
      cssClass: 'signupModal animated fadeInUp'
    });

    return await modal.present();
  }


}

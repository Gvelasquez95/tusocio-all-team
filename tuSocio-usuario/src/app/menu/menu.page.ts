import { Component, OnInit } from '@angular/core';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

  selectedPath = '';
  pages = [];

  public idSocio:any;
  public socioData:any;
  public logosocio:string;
  public razon:string;
  public fecha_venc:string;

  constructor(public socialSharing: SocialSharing, public alertCtrl: AlertController, private storage: Storage,
    public router:Router, public http: HttpClient) {

    this.pages = [
      {
        title: 'Perfil',
        icon: 'contact',
        action: 'goPerfil',
      },
      {
        title: 'Contactos',
        icon: 'contacts',
        action: 'goContactos',
      },
      {
        title: 'Mi plan',
        icon: 'bookmarks',
        action: 'goPlan',
      },
      {
        title: 'Nosotros',
        icon: 'information-circle',
        action: 'goAcerca',
      },
      {
        title: 'Compartir',
        icon: 'share',
        action: 'compartir',
      },
      {
        title: 'Configuración',
        icon: 'cog',
        action: 'goConfig',
      },
      {
        title: 'Salir',
        icon: 'log-out',
        action: 'salir',
      }
    ]
  }

  ngOnInit() {
  }

  public getAction(action:string){
    var act = action;
    console.log(act);
    
    if(act == 'goPerfil'){
      this.router.navigate(['menu/perfil']);

    }else if(act == 'goContactos'){
      this.router.navigate(['menu/favoritos']);

    }else if(act == 'goPlan'){
      this.router.navigate(['menu/plan']);

    }else if(act == 'goAcerca'){
      this.router.navigate(['menu/acerca']);

    }else if(act == 'compartir'){
      this.shareIt();

    }else if(act == 'goConfig'){
      this.router.navigate(['menu/config']);

    }else if(act == 'salir'){
      this.logOut();
    }else{
      //nothing
    }
  }

  public getDataUser(){
    this.http.get("https:/www.mundots.com/app_socio/app_1/lista_user.php?perfil&user="+this.idSocio).subscribe(data => {
      this.socioData = data;
      for( let udata of this.socioData){
          this.logosocio = udata.logo;
          this.razon = udata.razon_s;
          this.fecha_venc = udata.vence;
      }
    });
  }


  public shareIt(){
    this.socialSharing.share("Descubre un mundo lleno de oportunidades, por que somos tu Socio en los Negocios", null, null,"https://www.facebook.com/Tusocioapp/?epa=SEARCH_BOX")
    .then(res => console.log('',res))
    .catch(err => console.log('', err));
  }

  async logOut(){
    const logoutalert = await this.alertCtrl.create(
      {
        header: "Cerrar Sesión",
        message: "¿Esta seguro que desea cerrar sesión?",
        buttons:[
          {
            text: 'Cancelar',
            role: 'cancel',
            handler: () => {
              logoutalert.dismiss();
              return false;
            }
          },
          {
            text: 'Salir',
            handler: () => {
              this.storage.remove('idVendedor');
              this.storage.set('idVendedor',"");
              this.storage.remove('cargoVendedor');
              this.storage.set('cargoVendedor',"");
              this.storage.remove('idNegocio');
              this.storage.set('idNegocio',"");
              this.router.navigate(['/login']);
            }
          }
        ]
      });

      await logoutalert.present();
  }

}

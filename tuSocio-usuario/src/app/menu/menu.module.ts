import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MenuPage } from './menu.page';

const routes: Routes = [
  {
    path: '',
    component: MenuPage,
    children: [
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path: 'home', loadChildren: '../home/home.module#HomePageModule' },
      { path: 'perfil', loadChildren: '../perfil/perfil.module#PerfilPageModule' },
      { path: 'perfil/detalle', loadChildren: '../perfil/detalle/detalle.module#DetallePageModule' },
      { path: 'plan', loadChildren: '../plan/plan.module#PlanPageModule' },
      { path: 'favoritos', loadChildren: '../favoritos/favoritos.module#FavoritosPageModule' },
      { path: 'acerca', loadChildren: '../acerca/acerca.module#AcercaPageModule' },

      { path: 'oferta', loadChildren: '../oferta/oferta.module#OfertaPageModule' },
      { path: 'oferta/producto', loadChildren: '../oferta/producto/producto.module#ProductoPageModule' },
      { path: 'oferta/registrar', loadChildren: '../oferta/registrar/registrar.module#RegistrarPageModule' },
      { path: 'oferta/producto/detalle', loadChildren: '../oferta/producto/detalle/detalle.module#DetallePageModule' },
      { path: 'cotiza', loadChildren: '../cotiza/cotiza.module#CotizaPageModule' },
      { path: 'cotiza/registro-fase1', loadChildren: '../cotiza/registro-fase1/registro-fase1.module#RegistroFase1PageModule' },
      { path: 'cotiza/registro-fase2', loadChildren: '../cotiza/registro-fase2/registro-fase2.module#RegistroFase2PageModule' },
      { path: 'cotiza/detalle', loadChildren: '../cotiza/detalle/detalle.module#DetallePageModule' },
      { path: 'crece', loadChildren: '../crece/crece.module#CrecePageModule' },
      { path: 'crece/tutorial', loadChildren: '../crece/tutorial/tutorial.module#TutorialPageModule' },
      { path: 'crece/detalle', loadChildren: '../crece/detalle/detalle.module#DetallePageModule' },
      { path: 'crece/registro', loadChildren: '../crece/registro/registro.module#RegistroPageModule' },
      { path: 'mejora', loadChildren: '../mejora/mejora.module#MejoraPageModule' },
      { path: 'mejora/detalle', loadChildren: '../mejora/detalle/detalle.module#DetallePageModule' },
      { path: 'mejora/detalle/comprar', loadChildren: '../mejora/detalle/comprar/comprar.module#ComprarPageModule' },
      { path: 'socio', loadChildren: '../socio/socio.module#SocioPageModule' },
      { path: 'contable', loadChildren: '../contable/contable.module#ContablePageModule' },
      { path: 'contable/inventario', loadChildren: '../contable/inventario/inventario.module#InventarioPageModule' },
      { path: 'contable/inventario/detalle', loadChildren: '../contable/inventario/detalle/detalle.module#DetallePageModule' },
      { path: 'contable/inventario/registro', loadChildren: '../contable/inventario/registro/registro.module#RegistroPageModule' },
      { path: 'contable/inventario/servicio', loadChildren: '../contable/inventario/servicio/servicio.module#ServicioPageModule' },
      { path: 'contable/inventario/kardex', loadChildren: '../contable/inventario/kardex/kardex.module#KardexPageModule' },
      { path: 'contable/contabilidad', loadChildren: '../contable/contabilidad/contabilidad.module#ContabilidadPageModule' },
      { path: 'contable/proveedores', loadChildren: '../contable/proveedores/proveedores.module#ProveedoresPageModule' },
      { path: 'contable/proveedores/detalle', loadChildren: '../contable/proveedores/detalle/detalle.module#DetallePageModule' },
      { path: 'contable/proveedores/registro', loadChildren: '../contable/proveedores/registro/registro.module#RegistroPageModule' },
      { path: 'contable/ventas', loadChildren: '../contable/ventas/ventas.module#VentasPageModule' },
      { path: 'contable/ventas/carrito', loadChildren: '../contable/ventas/carrito/carrito.module#CarritoPageModule' },
      { path: 'contable/ventas/pagar', loadChildren: '../contable/ventas/pagar/pagar.module#PagarPageModule' },
      { path: 'contable/colaboradores', loadChildren: '../contable/colaboradores/colaboradores.module#ColaboradoresPageModule' },
      { path: 'contable/colaboradores/registro', loadChildren: '../contable/colaboradores/registro/registro.module#RegistroPageModule' },
      { path: 'chassky', loadChildren: '../chassky/chassky.module#ChasskyPageModule' },
      { path: 'chassky/home', loadChildren: '../chassky/home/home.module#HomePageModule' },
      { path: 'chassky/mapa', loadChildren: '../chassky/mapa/mapa.module#MapaPageModule' },
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MenuPage]
})
export class MenuPageModule {}

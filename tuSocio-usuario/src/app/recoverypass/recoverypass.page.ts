import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-recoverypass',
  templateUrl: './recoverypass.page.html',
  styleUrls: ['./recoverypass.page.scss'],
})
export class RecoverypassPage implements OnInit {

  constructor(public navCtrl: NavController) { }

  ngOnInit() {
  }

  public goBack(){
    this.navCtrl.navigateBack('/login');
  }

}

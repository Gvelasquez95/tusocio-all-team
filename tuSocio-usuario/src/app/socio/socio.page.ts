import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { Router, ActivatedRoute } from '@angular/router';
import { ActionSheetController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-socio',
  templateUrl: './socio.page.html',
  styleUrls: ['./socio.page.scss'],
})
export class SocioPage implements OnInit {

  public idSocio:string

  public dataSocio:any;
  //Parametros
  public socioFoto:string;
  public empresa:string;
  public empresario:string;
  public ruc:string;
  public descripcion:string;
  public telefono:string;
  public correo:string;
  public direccion:string;
  public web:string;

  constructor(public activatedRoute:ActivatedRoute, public http: HttpClient, public actionSheetCtrl: ActionSheetController,
    private socialSharing: SocialSharing,private callNumber: CallNumber, public router: Router, private iab: InAppBrowser) {
      
  }

  ngOnInit() {
    this.idSocio = this.activatedRoute.snapshot.paramMap.get('idSocio');
    this.getDataSocio(this.idSocio);
  }

  public getDataSocio(id:string){
    this.http.get("https://www.mundots.com/app_socio/app_1/lista_user.php?perfil&user="+id).subscribe(data => {
      this.dataSocio = data;
      
      for(let sdata of this.dataSocio){
        this.socioFoto = sdata.logo;
        this.empresa = sdata.razon_s;
        this.empresario = sdata.nombre;

        if(sdata.ruc == ""){
          if(sdata.tipo_doc == '1'){
            this.ruc = "DNI :" +sdata.nro_doc;
          }else if(sdata.tipo_doc == '2'){
            this.ruc = "RUC :" +sdata.nro_doc;
          }else{
            this.ruc = "CCE :" +sdata.nro_doc;
          }
        }else{
          this.ruc = "RUC: "+sdata.ruc;
        }

        this.descripcion = sdata.descripcion;
        this.telefono = sdata.telefono;
        this.correo = sdata.correo;
        this.direccion = sdata.direccion;
        this.web = sdata.web;
      }

    });

  }

  async openMenu(){
    const menu = await this.actionSheetCtrl.create({
      header: 'Opciones',
      buttons: [
      {
        text: 'LLamar',
        icon: 'call',
        handler: () => {
          this.callSocio();
        }
      }, {
        text: 'Contactar',
        icon: 'mail',
        handler: () => {
          this.sendEmail();
        }
      },{
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });

    await menu.present();
  }

  public callSocio(){
    this.callNumber.callNumber(this.telefono, true)
    .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));
  }

  public sendEmail(){
    this.socialSharing.shareViaEmail("","Contacto para ",[this.correo],null,null,null)
    .then(res => console.log('',res))
    .catch(err => console.log('', err));
  }

  public openWeb(){
    const browser = this.iab.create(this.web);
    browser.show();
  }

  public goPage(id:string){
    this.router.navigate(['/perfil/detalle', {idSocio:id}]);
  }

}

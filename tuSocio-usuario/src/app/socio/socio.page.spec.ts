import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SocioPage } from './socio.page';

describe('SocioPage', () => {
  let component: SocioPage;
  let fixture: ComponentFixture<SocioPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SocioPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocioPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Camera } from '@ionic-native/camera/ngx';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { LaunchNavigator } from '@ionic-native/launch-navigator/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { ChartsModule } from 'ng2-charts';
import { NegocioDataComponent } from './negocio-data/negocio-data.component';
import { ChasskymenuComponent } from './chasskymenu/chasskymenu.component';
import { LoginModalPageModule } from './login-modal/login-modal.module';
import { SignupModalPageModule } from './signup-modal/signup-modal.module';


@NgModule({
  declarations: [AppComponent, NegocioDataComponent, ChasskymenuComponent],
  entryComponents: [NegocioDataComponent, ChasskymenuComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(),
    AppRoutingModule,
    ChartsModule,
    LoginModalPageModule,
    SignupModalPageModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    CallNumber,
    SocialSharing,
    Camera,
    FileTransfer,
    LaunchNavigator,
    InAppBrowser,
    BarcodeScanner,
    Geolocation
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

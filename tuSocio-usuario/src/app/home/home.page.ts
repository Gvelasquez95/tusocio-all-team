import { Component, OnInit } from '@angular/core';
import { MenuController, ToastController, Events } from '@ionic/angular';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  public negocioData:any;

  public idSocio:string;
  public idNegocio:string;

  constructor(private menu: MenuController, public toastCtrl: ToastController, public router:Router, private storage: Storage,
    public http: HttpClient, public events: Events){
    this.menu.enable(true, 'menuTS');
  }

  ngOnInit(){
    this.storage.get("validaTSContable").then(data => {
      console.log("validaTSContable: "+data);
    });

    this.storage.get('idSocio').then(val => {
      this.idSocio = val;
      this.getDataNegocio(this.idSocio);
    });

    this.events.publish('getUserDataOnMenu');
  }

  ionViewWillEnter(){
    this.menu.enable(true, 'menuTS');
    this.storage.get("validaTSContable").then(data => {
      console.log("validaTSContable: "+data);
    });
    this.storage.set("validaTSContable","0");

    this.storage.get('idSocio').then(val => {
      this.idSocio = val;
      this.getDataNegocio(this.idSocio);
    })
  }

  public getDataNegocio(id:string){
    this.http.get("https://mundots.com/app_socio/app_1/lista_user.php?empresa&user="+id).subscribe(data => {
      this.negocioData = data;
      for(let empresa of this.negocioData){
        this.idNegocio = empresa.id;
        this.storage.set('idNegocio',this.idNegocio);
        console.log("idNegocio: "+this.idNegocio);
        
      }
    })
  }

  public getToast(){
    this.makeText("Pronto podras disfrutar de esta herramienta :)");
  }

  public goPage(pagina){
    if(pagina == '/chassky'){
      this.storage.get('showIntroSlider').then((v) => {
        if (v == 0) {
          this.router.navigate(['/chassky/home']);
        }
        else {
          this.router.navigate(['/chassky']);
        }
        },() => {
          this.router.navigate(['/chassky']);
      });
    }else{
      this.router.navigate([pagina]);
    }
  }

  async makeText(mensaje:string){
    const toast = await this.toastCtrl.create(
      {
        message: mensaje,
        duration: 2500
      }
    );
    await toast.present();
  }

}

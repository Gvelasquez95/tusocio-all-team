import { Component, OnInit } from '@angular/core';
import { Camera } from '@ionic-native/camera/ngx';
import { HttpClient } from '@angular/common/http';
import { ToastController, Events, ActionSheetController, LoadingController, NavController, AlertController } from '@ionic/angular';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { ActivatedRoute } from '@angular/router';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.page.html',
  styleUrls: ['./detalle.page.scss'],
})
export class DetallePage implements OnInit {

  public perfiles='socio';

  public idSocio:string;
  

  //Parametros SOCIO
  public userData:any;

  public fotoSocio:string="";
  public sociofilebase64:string="";

  public nomSocio:string="";
  public tipo_doc:string="";
  public nro_doc:string="";
  public fecha_nac:string="";
  public telefono:string="";
  public correo:string="";
  public descripcion:string="";
  public empresa:string="";

  //Parametros NEGOCIO
  public empresaData:any;

  public logoSocio:string="";
  public empresafilebase64:string="";

  public razon_s:string="";
  public ruc:string="";
  public tipo_negocio:string="";
  public tipo_rubro:string="";
  public direccionEmp:string="";
  public telefonoEmp:string="";
  public correoEmp:string="";
  public web:string="";

  constructor(private camera: Camera, private http: HttpClient, public toastCtrl: ToastController, public transfer: FileTransfer,
    public events: Events, public activatedRoute: ActivatedRoute, public actionSheetCtrl: ActionSheetController, public loadingCtrl: LoadingController,
    public navCtrl: NavController, public storage: Storage, public alertCtrl: AlertController) { 


  }

  ngOnInit() {
      this.storage.get('idSocio').then( val => {
        this.idSocio = val;
        console.log("Detalle Socio: "+this.idSocio);
        this.getSocioData(this.idSocio);
        this.getEmpresaData(this.idSocio);      
      })    
  }

  public segmentChanged(ev:any){
    this.perfiles = ev.detail.value;
    console.log(ev);
  }

  async addEmpresa(){
    const alertEmpresa = await this.alertCtrl.create(
      {
        header : 'Tu Socio',
        message: '¿Desea registrar su negocio?',
        backdropDismiss: false,
        buttons: [
          {
            text: 'OK',
            handler: () => {
              this.empresa = '1';
              this.perfiles = 'negocio';
            }
          },
          {
            text: 'Cancelar',
            role: "destructive",
            handler: () => {
              alertEmpresa.dismiss();
            }
          }
        ]
      });
     await alertEmpresa.present();
  }
  

  public getSocioData(id:string){
    this.http.get("https://www.mundots.com/app_socio/app_1/lista_user.php?perfil&user="+id).subscribe(data => {
      this.userData = data;
      console.log(this.userData);
      for(let socio of this.userData){
        this.fotoSocio = socio.foto;
        this.nomSocio = socio.nombre;
        this.tipo_doc = socio.tipo_doc;
        this.nro_doc = socio.nro_doc;
        this.fecha_nac = socio.fecha_nac;
        this.telefono = socio.telefono;
        this.correo = socio.correo;
        this.empresa = socio.empresa;
      }
    });
  }

  public getEmpresaData(id:string){
    this.http.get("https://mundots.com/app_socio/app_1/lista_user.php?empresa&user="+id).subscribe(data => {
      this.empresaData = data;

      for(let empresa of this.empresaData){
        this.logoSocio = empresa.logo;
        this.razon_s = empresa.razon_social;
        this.ruc = empresa.ruc;
        this.tipo_negocio = empresa.tipo_negocio;
        this.direccionEmp = empresa.direccion;
        this.telefonoEmp = empresa.telefono;
        this.correoEmp = empresa.correo;
        this.web = empresa.web;
        this.descripcion = empresa.descripcion;
      }
    })
  }

  async updateSocio(){

    console.log("filebase64socio :"+this.sociofilebase64);
    console.log("socio_"+ this.telefono);

    if(this.sociofilebase64 == ""){

          var url="https://www.mundots.com/app_socio/app_1/act_socio.php?perfil&user="+this.idSocio;

          var fs = new FormData();
          fs.append('user', this.idSocio);
          fs.append('nombre', this.nomSocio );
          fs.append('tipo_doc', this.tipo_doc);
          fs.append('nro_doc', this.nro_doc);
          fs.append('fecha_nac', this.fecha_nac);
          fs.append('correo', this.correo);
          fs.append('telefono', this.telefono);

          const loading = await this.loadingCtrl.create(
            {
              message: "Estamos actualizando su perfil..."
            }
          );

          await loading.present();
      
          this.http.post(url, fs).subscribe(data => {
          let result = JSON.parse(JSON.stringify(data || null ));
          let mensaje=result.status;
            
          console.log("data:"+data);
          

           if(mensaje == "success"){
            
            this.events.publish('update:perfil');
            this.makeText("¡Enhorabuena! Su perfil ha sido actualizado con éxito.");
            loading.dismiss();
            this.navCtrl.navigateRoot('/perfil');
          }
           else{
            this.makeText(mensaje);
          }
            this.loadingCtrl.dismiss();
          },err => {
            this.loadingCtrl.dismiss();
            this.makeText('Ocurrio un Error mientras se actuliazaba su información, intentelo nuevamente.');
         });

        }else{

          var url = "https://www.mundots.com/app_socio/app_1/act_socio.php?perfil&user="+this.idSocio;

          // File for Upload
          var targetPath = this.fotoSocio;

          var options = {
            fileKey: "foto",
            chunkedMode: false,
            mimeType: "multipart/form-data",
            params : {'user': this.idSocio,
                      'nombre': this.nomSocio,
                      'tipo_doc': this.descripcion,
                      'nro_doc': this.nro_doc,
                      'fecha_nac': this.fecha_nac,
                      'telefono': this.telefono,
                      'correo': this.correo},
            HttphttpMethod: 'POST'
          };

          const fileTransfer: FileTransferObject = this.transfer.create();

          const loading = await this.loadingCtrl.create(
            {
              message: "Estamos actualizando su perfil..."
            }
          );

          await loading.present();

          // Use the FileTransfer to upload the image
          fileTransfer.upload(targetPath, url, options).then(data => {
            if(data.response == 'success'){

              this.events.publish('update:perfil');
              this.makeText("¡Muy bien! Su perfil ha sido actualizado con éxito.");
              loading.dismiss();
              this.navCtrl.navigateRoot('/perfil');

            }else{
              loading.dismiss()
              this.makeText(data.response);
            }
          }, err => {
            loading.dismiss()
            this.makeText('Ocurrio un Error mientras se actuliazaba su información, intentelo nuevamente.');
          });
        }
  }

  async updateEmpresa(){

    if(this.empresafilebase64 == ""){

          var url="https://www.mundots.com/app_socio/app_1/act_socio.php?empresa&user="+this.idSocio;

          var fe = new FormData();
          fe.append('user', this.idSocio);
          fe.append('razon_s', this.razon_s);
          fe.append('ruc', this.ruc);
          fe.append('tipo_negocio',this.tipo_negocio);
          fe.append('tipo_rubro', this.tipo_rubro);
          fe.append('telefono', this.telefonoEmp);
          fe.append('correo', this.correoEmp);
          fe.append('descripcion', this.telefonoEmp);
          fe.append('direccion', this.direccionEmp);
          fe.append('web', this.web);

          const loading = await this.loadingCtrl.create(
            {
              message: "Estamos actualizando los datos de su negocio..."
            }
          );

          await loading.present();
      
          this.http.post(url, fe).subscribe(data => {
          let result = JSON.parse(JSON.stringify(data || null ));
          let mensaje=result.status;
      
           if(mensaje == "success"){
            
            this.events.publish('update:perfil');
            this.makeText("¡Muy bien! Los datos de tu negocio han sido actualizados.");
            loading.dismiss();
            this.navCtrl.navigateRoot('/perfil');
          }
           else{
            this.makeText(mensaje);
          }
            this.loadingCtrl.dismiss();
          },err => {

            this.loadingCtrl.dismiss();
            this.makeText('Ocurrio un Error mientras se actuliazaba su información, intentelo nuevamente.');
         });

        }else{

          var url = "https://www.mundots.com/app_socio/app_1/act_socio.php?empresa&user="+this.idSocio;

          // File for Upload
          var targetPath = this.logoSocio;

          var options = {
            fileKey: "logo",
            chunkedMode: false,
            mimeType: "multipart/form-data",
            params : {'user': this.idSocio,
                      'descripcion': this.descripcion,
                      'razon_s': this.razon_s,
                      'ruc': this.ruc,
                      'telefono': this.telefonoEmp,
                      'correo': this.correoEmp,
                      'direccion': this.direccionEmp,
                      'web': this.web
                     },
            HttphttpMethod: 'POST'
          };

          const fileTransfer: FileTransferObject = this.transfer.create();

          const loading = await this.loadingCtrl.create(
            {
              message: "Estamos actualizando su perfil..."
            }
          );

          await loading.present();

          // Use the FileTransfer to upload the image
          fileTransfer.upload(targetPath, url, options).then(data => {
            if(data.response == 'success'){

              this.events.publish('update:perfil');
              this.makeText("¡Enhorabuena! Su perfil ha sido actualizado con éxito.");
              loading.dismiss();
              this.navCtrl.navigateRoot('/perfil');

            }else{
              loading.dismiss()
              this.makeText(data.response);
            }
          }, err => {
            loading.dismiss()
            this.makeText('Ocurrio un Error mientras se actuliazaba su información, intentelo nuevamente.');
          });
        }
  }

  async seleccione() {
    const actionSheet = await this.actionSheetCtrl.create({
      header: 'Elija una opción',
      buttons: [
        {
          text: 'Galeria de Fotos',
          icon: 'image',
          handler: () => {
            this.openGallery();
          }
        },
        {
          text: 'Tomar Foto',
          icon: 'camera',
          handler: () => {
            this.takePhoto();
          }
        },
        {
          text: 'Cancelar',
          role: 'destructive',
          handler:()=>{
            actionSheet.dismiss();
          }
        }
      ]
    });
    await actionSheet.present();
  }

  public takePhoto() {

    this.camera.getPicture({
      quality: 50,
      sourceType: this.camera.PictureSourceType.CAMERA,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      allowEdit:true,
      saveToPhotoAlbum: true,
    }).then((imageData) => {

      if(this.perfiles == 'socio'){
        this.fotoSocio = "data:image/jpeg;base64," + imageData;
        this.sociofilebase64 = "data:image/jpeg;base64," + imageData;
      }else{
        this.logoSocio = "data:image/jpeg;base64," + imageData;
        this.empresafilebase64 = "data:image/jpeg;base64," + imageData;
      }
  
     }, (err) => {
      console.log(err);
    });
  }

  public openGallery(){
    this.camera.getPicture({
      quality: 50,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      allowEdit:true
    }).then((imageData) => {
      if(this.perfiles == 'socio'){
        this.fotoSocio = "data:image/jpeg;base64," + imageData;
        this.sociofilebase64 = "data:image/jpeg;base64," + imageData;
      }else{
        this.logoSocio = "data:image/jpeg;base64," + imageData;
        this.empresafilebase64 = "data:image/jpeg;base64," + imageData;
      }
     }, (err) => {
      console.log(err);
    });
  }

  async makeText(message){
    const toast = await this.toastCtrl.create({
      message: message,
      duration: 2500
    });
    await toast.present();
  }

}

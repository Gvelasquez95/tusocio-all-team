import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-negocio-data',
  templateUrl: './negocio-data.component.html',
  styleUrls: ['./negocio-data.component.scss'],
})
export class NegocioDataComponent implements OnInit {

  public idSocio:string;

  public empresaData:any;

  public logo_empresa:string;
  public razon_social:string;
  public ruc:string;

  constructor(public http: HttpClient, private storage: Storage) { }

  ngOnInit() {
    this.storage.get('idSocio').then(data => {
      this.idSocio = data;
      this.getDataEmpresa(this.idSocio);
    });
  }

  public getDataEmpresa(id:string){
    this.http.get("https://mundots.com/app_socio/app_1/lista_user.php?empresa&user="+id).subscribe(data => {
      this.empresaData = data;
      for(let empresa of this.empresaData){
        this.logo_empresa = empresa.logo;
        this.razon_social = empresa.razon_social;
        this.ruc = empresa.ruc;
      }
    });
  }

}

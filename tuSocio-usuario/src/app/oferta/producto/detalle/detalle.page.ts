import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ActionSheetController, AlertController, LoadingController, NavController } from '@ionic/angular';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { Button } from 'protractor';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.page.html',
  styleUrls: ['./detalle.page.scss'],
})
export class DetallePage implements OnInit {

  public cod_socio_visita:string;

  public ofertaData:any;
  public idOferta:string;
  //Parametros
  public idsocio:string;
  public ofertaFoto:string;
  public logo_user:string;
  public producto:string;
  public descripcion:string;
  public calificacion:string;
  public stock:string;
  public terminos:string;
  public razon:string;
  public nombre_socio:string;
  public telefono:string;
  public direccion:string;

  constructor(public activatedRoute: ActivatedRoute, private http: HttpClient, public actionSheetCtrl: ActionSheetController,
    private callNumber: CallNumber, public router: Router, public alertCtrl: AlertController, private storage: Storage,
    public loadingCtrl: LoadingController, public navCtrl: NavController) { }

  ngOnInit() {

    this.storage.get('idSocio').then( val => {
      this.cod_socio_visita = val;
    })

    this.idOferta = this.activatedRoute.snapshot.paramMap.get('idOferta');
    console.log("idOferta: "+this.idOferta);
    this.getDataOferta(this.idOferta);
  }

  public getDataOferta(id:string){
    this.http.get("https://www.cyhomework.com/app_socio/app_1/lista_oferta_id.php?idOferta="+id).subscribe( data => {
      this.ofertaData = data;
      console.log(this.ofertaData);
      
      for(let oferta of this.ofertaData){

        this.idsocio = oferta.socio;
        this.ofertaFoto = oferta.foto;
        this.logo_user = oferta.foto_empresa;
        this.producto = oferta.producto;
        this.descripcion = oferta.descripcion;
        this.calificacion = oferta.calificacion;
        this.stock = oferta.stock;
        this.terminos = oferta.terminos;
        this.razon = oferta.razon_s;
        this.nombre_socio = oferta.nombre;
        this.telefono = oferta.telefono;
        this.direccion = oferta.direccion;
      }
    });

  }

  async openMenu(){
    const menu = await this.actionSheetCtrl.create({
      header: 'Opciones',
      buttons: [
      {
        text: 'LLamar',
        icon: 'call',
        handler: () => {
          this.callSocio();
        }
      }, {
        text: 'Direccion',
        icon: 'pin',
        handler: () => {
          this.getMap();
        }
      },{
        text: 'Me Gusta',
        icon: 'star',
        handler: () => {
          this.like();
        }
      },{
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });

    await menu.present();
  }

  public visitProfile(idSocio:string){
    this.router.navigate(['/socio', {idSocio: idSocio}]);
  }

  public callSocio(){
    this.callNumber.callNumber(this.telefono, true)
    .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));
  }

  public like(){
    
  }

  public getMap(){

  }

  async extraFunctions(){

    const menu = await this.actionSheetCtrl.create({
      header: 'Opciones',
      buttons: [
      {
        text: 'Editar Oferta',
        icon: 'sync',
        handler: () => {
          
        }
      }, {
        text: 'Eliminar Oferta',
        icon: 'trash',
        role: 'destructive',
        handler: () => {   
          this.askforDelete(this.idOferta);
        }
      },{
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });

    await menu.present();
  }

  async askforDelete(idoferta:string){
    const dltalert = await this.alertCtrl.create(
      {
        header: "Importante",
        message: "¿Esta seguro que desea eliminar su oferta?. Una vez eliminada no podrá restaurarla",
        backdropDismiss: false,
        buttons: [
          {
            text: "Eliminar",
            role: "destructive",
            handler: () =>{
              
            }
          },
          {
            text: "Cancelar",
            handler: () =>{
              dltalert.dismiss();
            }
        }]
      })
    
      dltalert.present();
  }

  async deleteOferta(){
    var url="https://www.cyhomework.com/app_socio/app_1/registra_oferta.php";

    var fo = new FormData();
    fo.append('opcion', "delete");
    fo.append('idOferta', this.idOferta);

    const loading = await this.loadingCtrl.create(
      {
        message: "Estamos registrando tu información...",
        duration: 1500
      }
    );
    await loading.present();

    this.http.post(url, fo).subscribe(data => {
    let result = JSON.parse(JSON.stringify(data || null ));
    let mensaje=result.status;

     if(mensaje == "success"){
      this.makeText("Su oferta a sido eliminada.");
      this.navCtrl.navigateRoot('/oferta')
    }
     else{
      this.makeText(mensaje);
    }
      this.loadingCtrl.dismiss();
    },err => {
    this.loadingCtrl.dismiss();
    console.log(err);
   });
  }

  async makeText(mensaje:String){

  }

}

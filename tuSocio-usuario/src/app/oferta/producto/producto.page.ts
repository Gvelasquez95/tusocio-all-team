import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.page.html',
  styleUrls: ['./producto.page.scss'],
})
export class ProductoPage implements OnInit {

  public idCat = "";

  public listaOferta:any;
  public listaTipo:any;
  showFilters;

  public cadena:string="";
  public tipo:string="0";

  public descuento:string;

  constructor(public http:HttpClient, public router:Router, public loadingCtrl: LoadingController, public toastCtrl: ToastController, 
    public activatedRoute: ActivatedRoute) { 
    
  }

  public isToggle(){
    this.showFilters = !this.showFilters;
  }

  ngOnInit() {
    this.idCat = this.activatedRoute.snapshot.paramMap.get('idCat');
    
    this.checkListItems()
  }

  public getListaOfertasxCategoria(idCat){
    this.http.get("http://mundots.com/app_socio/app_1/lista_oferta_categoria.php?tipo_pro="+idCat).subscribe(data => {
      this.listaOferta = data;
      console.log(this.listaOferta);
      this.loadingCtrl.dismiss();
      // let i=0;
      // for (let i = 0; i < this.listaOferta.length; i++) {
      //   this.listaOferta[i]['nuevo_precio']='hola'+i;
      //   if(this.listaOferta[i].precio != ""){
      //     if(this.listaOferta[i].descuento != ""){
      //       let operacion= (this.listaOferta[i].precio - (this.listaOferta[i].precio * this.listaOferta[i].descuento / 100));
      //       if (operacion!=NaN) {
      //         this.listaOferta[i]['descuento']  ="S/. " +operacion.toString();
      //       }
            
      //      // this.descuento =  oferton.toString();
      //       console.log(this.listaOferta[i].precio);
      //     }else{
      //       this.listaOferta[i]['descuento'] = "S/. " + this.listaOferta[i].precio;
      //     }
      //   }else{
      //     this.listaOferta[i]['descuento'] = this.listaOferta[i].descuento + " %";
      //   }
      // }
      // console.log(this.listaOferta);

      // for(let oferta of this.listaOferta){

      //   if(oferta.precio != ""){
      //     if(oferta.descuento != ""){
      //       let oferton = (oferta.precio - (oferta.precio * oferta.descuento / 100));
      //       this.descuento = "S/. " + oferton.toString();
      //       console.log(this.descuento);
      //     }else{
      //       this.descuento = "S/. " + oferta.precio;
      //     }
      //   }else{
      //     this.descuento = oferta.descuento + " %";
      //   }
      //   i++;
      // }
     
    });
  }

  // public getListaTipo(){
  //   this.http.get("https://www.mundots.com/app_socio/app_1/lista_tipo.php").subscribe(data => {
  //     this.listaTipo = data;  
  //   });
  // }

  async busqueda(){

    var url="https://www.mundots.com/app_socio/app_1/busca_of.php?tipo="+this.tipo;
        
    var fo = new FormData();
    fo.append('cadena', this.cadena);
    //fo.append('tipo ', this.tipo);
    console.log(this.cadena+" "+this.tipo);
    
    
    const loading = await this.loadingCtrl.create(
      {
        message: "Buscando oferta...",
        spinner: "bubbles",
      }
    );
    await loading.present();

    this.http.post(url, fo).subscribe(data => {
      
      console.log(data);
      this.listaOferta = [];
      this.listaOferta = data;
    },err => {
      this.loadingCtrl.dismiss();
    console.log(err);
    });

  }

  addOferta(){
    this.router.navigate(['/menu/oferta/producto/registro']);
  }

  async checkListItems(){
    const loading = await this.loadingCtrl.create(
      {
        message: "Cargando productos...",
        mode: "ios",
        spinner: "bubbles",
        translucent: true,
        animated: true,
      }
    );

    await loading.present();

    this.getListaOfertasxCategoria(this.idCat);
  }

  irDetalle(idoferta:string){
    this.router.navigate(['/menu/oferta/producto/detalle',{idOferta:idoferta}]);
  }

  async makeText(mensaje:string){
    const toast = await this.toastCtrl.create({
      message: mensaje,
      duration: 2500
    });

    await toast.present();
  }


}

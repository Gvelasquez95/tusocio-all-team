import { Component, OnInit } from '@angular/core';
import { ActionSheetController, ToastController, LoadingController, NavController, Events } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';

import { FileTransferObject, FileTransfer } from '@ionic-native/file-transfer/ngx';
import { Storage } from '@ionic/storage';
import { Camera } from '@ionic-native/camera/ngx';

@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.page.html',
  styleUrls: ['./registrar.page.scss'],
})
export class RegistrarPage implements OnInit {

  public idSocio:string='';
  public fotoOferta:string='';

  public producto:string='';
  public descripcion:string='';

  public listaTipoProducto:any;
  public tipo_pro:string='';

  public terminos:string=''
  public precio;
  public descuento:number=0;
  public precio_final;

  constructor(public actionSheetCtrl: ActionSheetController, private camera: Camera, public toastCtrl: ToastController,public http: HttpClient,
    public loadingCtrl: LoadingController, private transfer: FileTransfer, public navCtrl: NavController, private storage: Storage,
    public events: Events) { }

  ngOnInit() {
    this.storage.get('idSocio').then(val => {
      this.idSocio = val
    });
    this.getListaTipoOferta();
  }

  public  getListaTipoOferta(){
    this.http.get("http://mundots.com/app_socio/app_1/lista_tipo.php").subscribe(data => {
      this.listaTipoProducto = data;
    });
  }

  async registrarOferta(){

    if(this.fotoOferta == ""){
      this.makeText("Te recomendamos agregar una foto a tu oferta!")
    }else{

    var url = "https://www.mundots.com/app_socio/app_1/registra_oferta.php?opcion=registra";

          // File for Upload
          var targetPath = this.fotoOferta;

          var options = {
            fileKey: "foto",
            chunkedMode: false,
            mimeType: "multipart/form-data",
            params : {'socio': this.idSocio,
                      'producto': this.producto,
                      'descripcion': this.descripcion,
                      'tipo': this.tipo_pro,
                      'terminos': this.terminos,
                      'precio': this.precio,
                      'descuento': this.descuento},
            HttphttpMethod: 'POST'
          };

          const fileTransfer: FileTransferObject = this.transfer.create();

          const loading = await this.loadingCtrl.create(
            {
              message: "Estamos registrando su oferta..."
            }
          );

          await loading.present();

          // Use the FileTransfer to upload the image
          fileTransfer.upload(targetPath, url, options).then(data => {
            if(data.response == 'success'){

              this.makeText("!Has registrado tu oferta con exito!");
              loading.dismiss();
              this.navCtrl.navigateRoot('/menu/oferta');
              this.events.publish('update:listaOferta');

            }else{
              loading.dismiss()
              this.makeText(data.response);
            }
          }, err => {
            loading.dismiss()
            this.makeText('Ocurrio un Error mientras se registraba su oferta, intentelo nuevamente.');
          });
        }
  }

  public setPrecioFinal(){
    if(this.precio != ''){
      if(this.descuento == 0){
        this.precio_final = this.precio;
      }else{
        this.precio_final = this.precio - (this.precio * this.descuento)/100;
      }
    }else if(this.descuento != 0){
      if(this.precio == ''){
        this.precio_final = "Solo descuento";
      }else{
        this.precio_final = this.precio - (this.precio * this.descuento)/100;
      }
    }else {
      this.precio_final = "0.00";
    }
  }

  async seleccione() {
    const actionSheet = await this.actionSheetCtrl.create({
      header: 'Elija una opción',
      buttons: [
        {
          text: 'Galeria de Fotos',
          icon: 'image',
          handler: () => {
            this.openGallery();
          }
        },
        {
          text: 'Tomar Foto',
          icon: 'camera',
          handler: () => {
            this.takePhoto();
          }
        },
        {
          text: 'Cancelar',
          role: 'destructive',
          handler:()=>{
            actionSheet.dismiss();
          }
        }
      ]
    });
    await actionSheet.present();
  }

  public takePhoto() {
    this.camera.getPicture({
      quality: 100,
      sourceType: this.camera.PictureSourceType.CAMERA,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      allowEdit:true,
      saveToPhotoAlbum: true,
    }).then((imageData) => {
      this.fotoOferta = "data:image/jpeg;base64," + imageData;
     }, (err) => {
      console.log(err);
    });
  }

  public openGallery(){
    this.camera.getPicture({
      quality: 100,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      allowEdit:true
    }).then((imageData) => {
      this.fotoOferta = "data:image/jpeg;base64," + imageData;
     }, (err) => {
      console.log(err);
    });
  }

  async makeText(message){
    const toast = await this.toastCtrl.create({
      message: message,
      duration: 2500
    });
    await toast.present();
  }

}


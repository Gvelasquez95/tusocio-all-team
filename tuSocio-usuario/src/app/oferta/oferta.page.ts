import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, NavController, Events, ToastController, LoadingController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { loadLContextFromNode } from '@angular/core/src/render3/discovery_utils';

@Component({
  selector: 'app-oferta',
  templateUrl: './oferta.page.html',
  styleUrls: ['./oferta.page.scss'],
})
export class OfertaPage implements OnInit {

  public opciones='oferta';
  public seleccion='todo';
  public listaCategorias:any;

  public listaOferta:any=[];


  //Segment Mis Ofertas

  public listaMisOfertas:any;
  public idSocio:string;

  constructor(public router: Router, public navCtrl: NavController, public http: HttpClient, private storage: Storage,
    public alertCtrl:AlertController, public events: Events, public toastCtrl: ToastController, public loadingCtrl: LoadingController) { }

  ngOnInit() {
    this.getAllListaOferta();

    this.storage.get('idSocio').then(val => {
      this.idSocio = val;
      this.getlistaOfertaxSocio(this.idSocio);
  });

    this.listaCategorias = [
      {
        idCat: '1',
        image: '../../assets/images/cat1.png',
        titulo: 'Restaurantes',
      },
      {
        idCat: '2',
        image: '../../assets/images/cat2.png',
        titulo: 'Bodegas'
      },
      {
        idCat: '3',
        image: '../../assets/images/cat3.png',
        titulo: 'Licorerias'
      },
      {
        idCat: '4',
        image: '../../assets/images/cat4.png',
        titulo: 'Farmacias'
      },
      {
        idCat: '5',
        image: '../../assets/images/cat5.png',
        titulo: 'Florerias'
      },
      {
        idCat: '6',
        image: '../../assets/images/cat6.png',
        titulo: 'Lavanderias'
      },
      {
        idCat: '7',
        image: '../../assets/images/cat7.png',
        titulo: 'Servicios'
      },
      {
        idCat: '8',
        image: '../../assets/images/cat8.png',
        titulo: 'Panaderias y Pastelerías'
      }
    ]
  }

  ionViewWillEnter(){
    this.events.subscribe('update:listaOferta', () => {
      this.getlistaOfertaxSocio(this.idSocio);
    })
  }

  public getAllListaOferta(){
    this.http.get("http://mundots.com/app_socio/app_1/lista_oferta.php").subscribe(data => {
      this.listaOferta = data;
    });
  }

  public getlistaOfertaxSocio(idSocio){
    this.http.get("http://mundots.com/app_socio/app_1/lista_oferta_socio.php?socio="+idSocio).subscribe(data => {
      this.listaMisOfertas = data;
      console.log(this.listaMisOfertas);
      if(this.listaMisOfertas.length == 0){
        this.tieneOferta();
      }
    });
  }

  addOferta(){
    this.navCtrl.navigateForward('/menu/oferta/registrar')
  }

  async tieneOferta(){
    const alert = await this.alertCtrl.create({
      backdropDismiss: false,
      header: "Tu Socio",
      message: "Por el momento no has registrado ninguna oferta, ¿Te gustaría registrar alguna oferta?",
      buttons: [
        {
          text: "Ahora no",
          role: "destructive",
          handler: () => {
            alert.dismiss();
          }
        },
        {
          text: "Registrar",
          handler: () => {
            this.opciones = 'mis_ofertas'
            this.navCtrl.navigateBack('/menu/oferta/registrar');
          }
        },
      ]
    });
    await alert.present();
  }

  public goPage(cod_cat:string){
    this.router.navigate(['/menu/oferta/producto', {idCat: cod_cat}]);
  }

  public goDetailMisOfertas(ofertaId:string){
    this.router.navigate(['/menu/oferta/producto/detalle' , {idOferta: ofertaId}]);
  }

  public segmentChanged(ev:any){
    this.opciones = ev.detail.value;
    console.log(ev);
  }

  public segmentChanged2(ev:any){
    this.seleccion = ev.detail.value;
    console.log(ev);
  }

  async deleteItem(idoferta:string, producto:string){
    const alert = await this.alertCtrl.create({
      header: "Eliminar Oferta",
      backdropDismiss: false,
      message: "¿Esta seguro que desea eliminar "+ producto +" de su lista de ofertas?",
      mode: "ios",
      buttons: [
        {
          text: 'Eliminar',
          role: 'destructive',
          handler: () => {
            this.eliminarOferta(idoferta);
          }
        },
        {
          text: 'Cancelar',
          handler: () => {
            this.alertCtrl.dismiss();
          }
        }
      ]
    });
    await alert.present();
  }

  async eliminarOferta(id:string){
    var url = "https://mundots.com/app_socio/app_1/eliminar_oferta.php?";

    var fo = new FormData();
    fo.append('idOferta', id);

    const loading = await this.loadingCtrl.create(
      {
        message: "Eliminando oferta...",
        spinner: "bubbles",
        backdropDismiss: false
      }
    );
    await loading.present();

    this.http.post(url,fo).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data || null ));
      let mensaje=result.status;
  
       if(mensaje == "success"){
        this.makeText("Su oferta ha sido eliminada");
        this.getlistaOfertaxSocio(this.idSocio);
      }
       else{
        this.makeText(mensaje);
      }
        this.loadingCtrl.dismiss();
      },err => {
      this.loadingCtrl.dismiss();
      console.log(err);
     });
    }
  
  async makeText(mensaje:string){
    const toast = await this.toastCtrl.create(
      {
        message: mensaje,
        duration: 2000
      }
    );

    await toast.present();
  }  

}

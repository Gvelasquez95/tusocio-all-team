import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController, LoadingController, AlertController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-login-modal',
  templateUrl: './login-modal.page.html',
  styleUrls: ['./login-modal.page.scss'],
})
export class LoginModalPage implements OnInit {

  public socioData:any;

  public usuario:string;
  public clave:string;
  public validacion:string;
  public ischecked:boolean=true;

  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';
  
  constructor(public modalCtrl: ModalController, public toastCtrl: ToastController, public http: HttpClient,
    public router: Router, public loadingCtrl: LoadingController, private storage: Storage, public alertCtrl: AlertController) { }

  ngOnInit() {
  }

  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  public goPage(pagina){
    this.router.navigate([pagina]);
  }

  async login(){

    var url="https://www.mundots.com/app_socio/app_1/login.php?log";
    console.log(this.ischecked);
    var fl = new FormData();
    fl.append('user', this.usuario);
    fl.append('pass', this.clave);
    console.log(this.usuario +this.clave);
    
    const loading = await this.loadingCtrl.create(
      {
        message: "Verificando Datos...",
        duration: 2000
      }
    );
    await loading.present();

    this.http.post(url, fl).subscribe(data => {
    let result = JSON.parse(JSON.stringify(data || null ));
    let mensaje=result.status;

    if(mensaje == "success"){
    this.http.get("https://www.mundots.com/app_socio/app_1/login.php?login&user="+this.usuario+"&pass="+this.clave).subscribe(data => {
      this.socioData = data;
        for( let udata of this.socioData){
          this.storage.set('idSocio',udata.idSocio);
          this.storage.set('planSocio',udata.plan);
          this.storage.set('isChecked',this.ischecked);
          this.makeText("Bienvenido(a) "+udata.nombre);
          this.modalCtrl.dismiss();
          if(udata.estado == '2'){
            this.router.navigate(['menu/home']);
          }else if (udata.estado == '1'){
            this.router.navigate(['menu/perfil/detalle']);
            this.alertMessage("Importante:","Se le recomienda completar los datos a continuación, con la finalidad de una mejor experiencia para el usuario.Atte. Equipo Tu Socio");
          }
        }
      });
    }
    else{
      this.makeText(mensaje);
    }
      this.loadingCtrl.dismiss();
    },err => {
      this.loadingCtrl.dismiss();
    console.log(err);
    });
  }

  async makeText(mensaje:string){
    const toast = await this.toastCtrl.create(
      {
        message: mensaje,
        duration: 2500
      }
    )
    await toast.present()
  }

  async alertMessage(header:string,mensaje:string){
    const alert = await this.alertCtrl.create({
      header: header,
      message: mensaje,
      backdropDismiss: false,
      buttons: [
        {
          text: 'Entendido',
          handler: () => {
            alert.dismiss();
            return false;
          }
        }
      ]
    })

    await alert.present();  
  }

  public close(){
    this.modalCtrl.dismiss();
  }

}

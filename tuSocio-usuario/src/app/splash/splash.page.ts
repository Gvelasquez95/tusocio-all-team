import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { MenuController, AlertController, NavController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-splash',
  templateUrl: './splash.page.html',
  styleUrls: ['./splash.page.scss'],
})
export class SplashPage implements OnInit {

  public listsirve:any;

  constructor(private storage:Storage, public navCtrl: NavController, public menu: MenuController, public alertCtrl: AlertController,
    public http: HttpClient) {
    this.menu.enable(false);
   }

  ngOnInit() {
    //this.valida();
    this.sesion();
  }

  public valida(){
    this.http.get("https://www.mundots.com/app_socio/app_1/valida.php").subscribe(data => {
      this.listsirve = data;
        for( let sirve of this.listsirve){
            if (sirve.opcion!='2') {
              this.makeAlert2();
            }else{
              this.sesion();
            }
        }
      });
  }

  public sesion(){
    this.storage.get('isChecked').then((val) => {
      if (val==true) {
        this.storage.get('idSocio').then((val) => {
          if (val!=null) {
            console.log(val);
            setTimeout(() => this.navCtrl.navigateRoot('/menu/home'), 3000);
          }else{
            console.log(val);
            setTimeout(() => this.navCtrl.navigateRoot('/login'), 3000);
          }
        });
      }else{
        console.log(val);
        setTimeout(() => this.navCtrl.navigateRoot('/login'), 3000);
      }
    });
  }

  async makeAlert2(){
    const alert = await this.alertCtrl.create({
      header: "Aplicacion desactualizada:",
      message: "La aplicacion debe ser actualizada para poder seguir usandose, verifique que tiene la ultima version por favor.",
      backdropDismiss: false,
      buttons: [
        {
          text: 'Entendido',
          handler: () => {
            navigator['app'].exitApp();
          }
        }
      ]
  });

    await alert.present();
  }


}

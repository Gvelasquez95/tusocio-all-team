import { Component, OnInit } from '@angular/core';
import { NavController, ToastController, LoadingController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  //Parametros
  public nombre:string;
  public correo:string;
  public telefono:string;
  public clave:string;

  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';

  constructor(public navCtrl: NavController,public http: HttpClient,public toastCtrl: ToastController,
    public loadingCtrl: LoadingController) { }

  ngOnInit() {
  }

  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  public goBack(){
    this.navCtrl.navigateBack('/login');
  }

  async register(){
    var url="https://www.mundots.com/app_socio/app_1/registro.php";

    var fs = new FormData();
    fs.append('nombre', this.nombre);
    fs.append('correo', this.correo);
    fs.append('telefono', this.telefono);
    fs.append('clave', this.clave);

    const loading = await this.loadingCtrl.create(
      {
        message: "Estamos registrando tu información...",
        duration: 1500
      }
    );
    await loading.present();

    this.http.post(url, fs).subscribe(data => {
    let result = JSON.parse(JSON.stringify(data || null ));
    let mensaje=result.status;

     if(mensaje == "success"){
      this.makeText("Bienvenido a Tu Socio.");
      this.navCtrl.navigateBack('/login');
    }
     else{
      this.makeText(mensaje);
    }
      this.loadingCtrl.dismiss();
    },err => {
    this.loadingCtrl.dismiss();
    console.log(err);
   });
  }

  async makeText(mensaje:string){
    const toast = await this.toastCtrl.create(
      {
        message: mensaje,
        duration: 2500
      }
    );
    await toast.present();
  }


}

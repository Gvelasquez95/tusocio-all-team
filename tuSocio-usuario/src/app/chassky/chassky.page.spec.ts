import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChasskyPage } from './chassky.page';

describe('ChasskyPage', () => {
  let component: ChasskyPage;
  let fixture: ComponentFixture<ChasskyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChasskyPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChasskyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';

import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LoadingController } from '@ionic/angular';

declare var google;

@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.page.html',
  styleUrls: ['./mapa.page.scss'],
})
export class MapaPage implements OnInit {

  mapRef = null;

  markers: any[] = [
    {
      position:{
        latitude: -12.0945717,
        longitude: -77.046638,
      },
      title:'Point 1'
    },
    {
      position:{
        latitude: -12.094045,
        longitude: -77.044919,
      },
      title:'Point 2'
    },
    {
      position:{
        latitude: -17.391398,
        longitude: -66.2407904,
      },
      title:'Point 3'
    },
    {
      position:{
        latitude: -17.3878887,
        longitude: -66.223664,
      },
      title:'Point 4'
    },
  ];

  constructor(public geo: Geolocation, public loadingCtrl:LoadingController) { }

  ngOnInit(){
    this.loadMap();
  }

  async loadMap(){

    const loading = await this.loadingCtrl.create({
      message: "Cargando Mapa...",
      spinner: "lines",
      translucent: true,
    });
    await loading.present();

    const coordinates = await this.getLocation();

    const mapEle: HTMLElement = document.getElementById('map');
    this.mapRef = new google.maps.Map(mapEle, {
      center: coordinates,
      zoom: 13
    });

    google.maps.event.addListenerOnce(this.mapRef, 'idle' , () => {
      console.log('added');
      loading.dismiss();

      this.addMarker(coordinates.lat,coordinates.lng,'Yo');
      this.addCircle(coordinates.lat,coordinates.lng,
        this.markers.forEach(marker=>{
          this.addMarker(marker.position.latitude,marker.position.longitude,marker.title);
        })
      );


    })
  };

  private async addMarker(lat: number,lng:number,title:string){
    var image = {
      url: '../../../assets/images/logo.png',
      scaledSize: new google.maps.Size(50, 50)
    }

    const marker = new google.maps.Marker({
      position:{lat, lng},
      icon: image,
      animation: google.maps.Animation.DROP,
      draggable: true,
      options: {
        labelAnchor: "36 61",
         labelInBackground: false
      }
    });

    const infowindow = new google.maps.InfoWindow({
      content: title,
      maxWidth: 400
    });

    marker.addListener('click', function() {
      infowindow.open(this.mapRef, marker);
    });

    await marker.setMap(this.mapRef);
  }

  private async addCircle(lat: number,lng:number, marker:any){
    const circle = new google.maps.Circle({
      center: {lat, lng},
      radius: 1000,
      strokeColor : '#43AEFF',
      strokeWidth: 1,
      fillColor: '#43AEFF'
    });
    await circle.setMap(this.mapRef);
    return (circle.getBounds());
  }

  private async getLocation(){
    const location = await this.geo.getCurrentPosition();
    return {
      lat: location.coords.latitude,
      lng: location.coords.longitude  
    };
  }

}

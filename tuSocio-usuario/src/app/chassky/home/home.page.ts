import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import { AlertController, PopoverController, ToastController, LoadingController } from '@ionic/angular';
import { ChasskymenuComponent } from 'src/app/chasskymenu/chasskymenu.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  interval;

  public listaCategorias:any = [];
  public dataSocio:any=[];

  public idSocio:string="";
  public nombreSocio:string="";
  public telefonoSocio:string="";
  public correoSocio:string="";
  public direccionSocio:string="";

  //Parametro de Tiempo
  public HourOfDay;

  constructor(private storage: Storage, public http: HttpClient, public alertCtrl: AlertController, public popoverCtrl: PopoverController,
    public toastCtrl: ToastController, public loadingCtrl: LoadingController, public router:Router) { }

  async ngOnInit() {
    this.storage.get('idSocio').then(data => {
      this.idSocio = data;
      this.getDataSocio(this.idSocio);
    })

    this.listaCategorias = [
      {
        imagen: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTl_BmBngoRS266vBrRFQn9GBPn37C1olo5XR_V1pL0RwFIfwkH',
        descripcion: 'Comida'
      },
      {
        imagen: 'https://vignette.wikia.nocookie.net/mario/images/e/eb/Bloque_%3F.jpeg/revision/latest?cb=20101203064455&path-prefix=es',
        descripcion: 'Suerte'
      }
    ]

    this.getDayTime();

    this.interval = setInterval(() => {
        this.getDayTime();
    }, 3000); 

  }

  ionViewWillLeave(){
    clearInterval(this.interval);
  }


  public getDataSocio(socio:string){
    this.http.get("https://www.mundots.com/app_socio/app_1/lista_user.php?perfil&user=1").subscribe(data => {
      this.dataSocio = data;
      for(let socio of this.dataSocio){
        this.nombreSocio = socio.nombre;
        this.telefonoSocio = socio.telefono;
        this.correoSocio = socio.correo;
        this.direccionSocio = socio.direccion;
      }
    })
  }

  async checkData(){
    const alert = await this.alertCtrl.create ({
      header:"Verifica tus datos porfavor",
      backdropDismiss: true,
      animated: true,
      mode: "ios",
      translucent: false,
      inputs: [
        {
          name: 'correo',
          type: 'email',
          placeholder: 'Ingrese su correo',
          value: this.correoSocio
        },
        {
          name: 'telefono',
          type: 'number',
          placeholder: 'Ingrese su telefono',
          value: this.telefonoSocio
        },
        {
          name: 'direccion',
          type: 'text',
          placeholder: 'Ingrese su direccíon'
        }
      ],
      buttons: [
        {
          text: 'Hecho',
          handler: data => {
            return false;
          }          
        }
      ]   
    });

    await alert.present();
  }

  async showMenu(ev: any) {
    const popover = await this.popoverCtrl.create({
      component: ChasskymenuComponent,
      event: ev,
      translucent: false,
      mode: 'ios',
      animated: true,
      backdropDismiss: true
    });
    return await popover.present();
  }

  public getDayTime(){
    let date = new Date();
    let hour = date.getHours();
    console.log(hour);
    
    if(hour > 0 && hour < 6){
      this.makeText('Madrugada');  
      this.HourOfDay = 'madrugada';
    }else if(hour >= 6 && hour <= 12){
      this.makeText('Mañana');  
      this.HourOfDay = 'dia'
    }else if(hour >= 12 && hour <= 16){
      this.makeText('Buenas tardes');  
      this.HourOfDay = 'tarde'
    }else if(hour >= 16 && hour <= 24){
      this.makeText('Buenas noches'); 
      this.HourOfDay = 'noche'
    }

  }

  async makeText(mensaje:string){
    const toast = await this.toastCtrl.create(
      {
        message: mensaje,
        duration: 1500
      }
    )

    await toast.present();
  }

  goMap(){
    this.router.navigate(['/chassky/mapa']);
  }

}

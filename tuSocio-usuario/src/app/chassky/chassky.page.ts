import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';


@Component({
  selector: 'app-chassky',
  templateUrl: './chassky.page.html',
  styleUrls: ['./chassky.page.scss'],
})
export class ChasskyPage implements OnInit {

  public options;

  constructor(private router: Router,private storage: Storage){
      this.options = {
          show: false
      };
  }

  ngOnInit(){
    
  }
 
  completeSlider() {
    if (!this.options.show) {
        this.storage.set('showIntroSlider', 0).then(() => {
        });
    }
    this.router.navigate(['/chassky/home']);
  }

}

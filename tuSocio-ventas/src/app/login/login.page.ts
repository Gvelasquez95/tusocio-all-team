import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from "@angular/common/http";
import { Storage } from '@ionic/storage';
import { LoadingController, ToastController, AlertController, Platform } from '@ionic/angular';
import { Network } from '@ionic-native/network/ngx';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  
  public vendedorData:any;
  public usuario:string;
  public clave:string;
  public idVendedor:string;
  public ischecked:boolean=true;

  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';

  constructor(public router: Router, public http:HttpClient, private storage:Storage, public loadingCtrl: LoadingController,
    public toastCtrl:ToastController, public alertCtrl: AlertController) { 

    this.ischecked = true;

  }

  ngOnInit() {
    
  }

  public hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  async login(){
    var url="http://mundots.com/app_socio/app_2/login.php?log ";
        
    var fl = new FormData();
    fl.append('user', this.usuario);
    fl.append('pass', this.clave);
    
    const loading = await this.loadingCtrl.create(
      {
        message: "Verificando Datos...",
        duration: 2000
      }
    );
    await loading.present();

    this.http.post(url, fl).subscribe(data => {
    let result = JSON.parse(JSON.stringify(data || null ));
    let mensaje=result.status;

    if(mensaje == "success"){
    this.makeText("Bienvenido");
    this.http.get("http://mundots.com/app_socio/app_2/login.php?login&user="+this.usuario+"&pass="+this.clave).subscribe(data => {
      this.vendedorData = data;
        for( let udata of this.vendedorData){
          this.storage.set('idVendedor',udata.id);
          this.storage.set('cargoVendedor',udata.tipo);
          this.storage.set('isChecked',this.ischecked);
          this.router.navigate(['/home'])
        }
      });
    }
    else{
      this.makeText(mensaje);
    }
      this.loadingCtrl.dismiss();
    },err => {
      this.loadingCtrl.dismiss();
    console.log(err);
    });
  }

  async makeText(mensaje:string){
    const toast = await this.toastCtrl.create(
      {
        message: mensaje,
        duration: 2500
      }
    )
    await toast.present()
  }

  public goPage(pagina){
    this.router.navigate([pagina]);
  }

}

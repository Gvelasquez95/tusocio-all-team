import { Component, OnInit } from '@angular/core';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { Storage } from '@ionic/storage';
import { Camera } from '@ionic-native/camera/ngx';
import { ActionSheetController, ToastController, LoadingController, NavController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-add-afiliador',
  templateUrl: './add-afiliador.page.html',
  styleUrls: ['./add-afiliador.page.scss'],
})
export class AddAfiliadorPage implements OnInit {

  public idVendedor:string;
  public cod_tipo:string;
  //Parametros
  public foto_vendedor:string;
  public nombre:string;
  public tipo_documento:string;
  public nro_documento:string;
  public telefono:string;
  public correo:string;
  public clave:string;

  constructor(private photoViewer: PhotoViewer, private storage: Storage,private camera: Camera,public http: HttpClient,
    public actionSheetCtrl: ActionSheetController, public toastCtrl: ToastController, public loadingCtrl: LoadingController,
    public navCtrl: NavController, public activatedRoute: ActivatedRoute) { 

  }

  ngOnInit() {

    this.cod_tipo = this.activatedRoute.snapshot.paramMap.get('cod_tipo');

    this.storage.get('idVendedor').then(val => {
      this.idVendedor = val;
      console.log(this.idVendedor);
      
    });
  }

  public registrar(){
    if(this.cod_tipo == '1'){
      this.registrarAfiliador()
    }else if(this.cod_tipo == '3'){
      this.registrarLider()
    }
  }

  async registrarAfiliador(){
    var url="http://mundots.com/app_socio/app_2/registra_equipo.php?user="+this.idVendedor;

    var fa = new FormData();
    fa.append('user', this.idVendedor);
    fa.append('nombre', this.nombre);
    fa.append('tipo_doc', this.tipo_documento);
    fa.append('nro_doc', this.nro_documento);
    fa.append('correo', this.correo);
    fa.append('telefono', this.telefono);
    fa.append('clave', this.clave);

    const loading = await this.loadingCtrl.create(
      {
        message: "Realizando registro...",
        duration: 1500
      }
    );
    await loading.present();

    this.http.post(url, fa).subscribe(data => {
    let result = JSON.parse(JSON.stringify(data || null ));
    let mensaje=result.status;

     if(mensaje == "success"){
      this.makeText("!Enhorabuena! Has agregado un nuevo miembro a tu equipo");
      this.navCtrl.navigateBack('/home');
    }
     else{
      this.makeText(mensaje);
    }
      this.loadingCtrl.dismiss();
    },err => {
    this.loadingCtrl.dismiss();
    console.log(err);
   });
  }

  async registrarLider(){
    var url="http://mundots.com/app_socio/app_2/lider.php?registra&user="+this.idVendedor;

    var fa = new FormData();
    fa.append('user', this.idVendedor);
    fa.append('nombre', this.nombre);
    fa.append('tipo_doc', this.tipo_documento);
    fa.append('nro_doc', this.nro_documento);
    fa.append('correo', this.correo);
    fa.append('telefono', this.telefono);
    fa.append('clave', this.clave);

    const loading = await this.loadingCtrl.create(
      {
        message: "Realizando registro...",
        duration: 1500
      }
    );
    await loading.present();

    this.http.post(url, fa).subscribe(data => {
    let result = JSON.parse(JSON.stringify(data || null ));
    let mensaje=result.status;

     if(mensaje == "success"){
      this.makeText("!Enhorabuena! Has agregado un nuevo lider a tu equipo");
      this.navCtrl.navigateBack('/home');
    }
     else{
      this.makeText(mensaje);
    }
      this.loadingCtrl.dismiss();
    },err => {
    this.loadingCtrl.dismiss();
    console.log(err);
   });
  }

  public showFoto(foto:string){
    this.photoViewer.show(foto);
  }

  async seleccione() {
    const actionSheet = await this.actionSheetCtrl.create(
    {
      header: 'Elija una opción',
      animated: true,
      buttons: [
        {
          text: 'Galeria de Fotos',
          icon: 'images',
          handler: () => {
            this.openGallery();
          }
        },
        {
          text: 'Tomar Foto',
          icon: 'camera',
          handler: () => {
            this.takePhoto();
          }
        },
        {
          text: 'Cancelar',
          role: 'destructive',
          icon: 'close',
          handler: () => {
            actionSheet.dismiss();
            return false;
          }
        }
      ]
    });
    await actionSheet.present();
  }

  public takePhoto() {
    this.camera.getPicture({
      quality: 100,
      sourceType: this.camera.PictureSourceType.CAMERA,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      allowEdit:true,
      saveToPhotoAlbum: true,
    }).then((imageData) => {
      this.foto_vendedor = "data:image/jpeg;base64," + imageData;
     }, (err) => {
      console.log(err);
    });
  }

  public openGallery(){
    this.camera.getPicture({
      quality: 100,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      allowEdit:true
    }).then((imageData) => {
      this.foto_vendedor = "data:image/jpeg;base64," + imageData;
     }, (err) => {
      console.log(err);
    });
  }

  async makeText(mensaje:string){
    const toast = await this.toastCtrl.create(
      {
        message: mensaje,
        duration: 2500
      }
    );
    await toast.present();
  }

}

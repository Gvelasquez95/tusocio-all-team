import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAfiliadorPage } from './add-afiliador.page';

describe('AddAfiliadorPage', () => {
  let component: AddAfiliadorPage;
  let fixture: ComponentFixture<AddAfiliadorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAfiliadorPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAfiliadorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

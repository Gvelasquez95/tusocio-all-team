import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { AddAfiliadorPage } from './add-afiliador.page';

const routes: Routes = [
  {
    path: '',
    component: AddAfiliadorPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AddAfiliadorPage]
})
export class AddAfiliadorPageModule {}

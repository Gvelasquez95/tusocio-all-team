import { Component } from '@angular/core';

import { Platform, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Network } from '@ionic-native/network/ngx';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import * as firebase from 'firebase/app';
import { firebaseConfig } from './credentials';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {

 
  constructor(private platform: Platform, private splashScreen: SplashScreen, private statusBar: StatusBar,
    private permissions: AndroidPermissions, private network: Network, public http:HttpClient,public router: Router,
    public alertCtrl: AlertController) {
      
    var listaPermisos = [
      permissions.PERMISSION.ACCESS_FINE_LOCATION,
      permissions.PERMISSION.CAMERA,
      permissions.PERMISSION.SEND_SMS,
      permissions.PERMISSION.READ_CONTACTS,
      permissions.PERMISSION.CALL_PHONE
    ];

    this.permissions.requestPermissions(listaPermisos);

    this.initializeApp();

  }

  initializeApp() {
      firebase.initializeApp(firebaseConfig);
      this.platform.ready().then(() => {
        
        if (this.network.type === 'none') {
          this.makeAlert();
        }else{
          //nothing
          console.log(this.network.type);
        }
     
    });
  }

  async makeAlert(){
    const alert = await this.alertCtrl.create({
      header: "Error de Conexion:",
      message: "No se ha podido conectar con el servidor. Compruebe tu conexión a Internet y vuelve a intentarlo.",
      backdropDismiss: false,
      buttons:[
        {
          text: 'Reintentar',
          handler: () => {
            window.location.reload();
          }
        }
      ]
  });
    await alert.present();
  }

}

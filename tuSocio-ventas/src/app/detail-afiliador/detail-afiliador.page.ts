import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-detail-afiliador',
  templateUrl: './detail-afiliador.page.html',
  styleUrls: ['./detail-afiliador.page.scss'],
})
export class DetailAfiliadorPage implements OnInit {

  public miembroData:any;

  public idMiembro:string;
  public foto_miembro:string;
  public nombre:string;
  public cargo:string;
  public puesto:string;
  public habilidades:string;
  public telefono:string;
  public correo:string;
  public direccion:string;

  constructor(public activatedRoute: ActivatedRoute, public http: HttpClient, public callnumber: CallNumber,
    public socialSharing: SocialSharing) { }

  ngOnInit() {
    this.idMiembro = this.activatedRoute.snapshot.paramMap.get('idMiembro');
    console.log("idMiembro: "+this.idMiembro);
    this.getDataMiembro(this.idMiembro);
  } 

  public getDataMiembro(miembro:string){
    this.http.get("http://mundots.com/app_socio/app_2/lista_user.php?perfil&user="+miembro).subscribe(data => {
      this.miembroData = data;
        for( let mdata of this.miembroData){

            if(mdata.foto == ''){
              this.foto_miembro = "../../assets/images/logo_bg_blanco.png";
            }else{
              this.foto_miembro = mdata.foto;
            }

            this.nombre = mdata.nombre;
            this.correo = mdata.correo;
            this.telefono = mdata.telefono;
            this.habilidades = mdata.habilidades;

            if(mdata.tipo == "2"){
              this.puesto = "Empresario Independiente";
              this.cargo = "Afiliador";
            }
        }
    });
  }

  public callMiembro(fono:string){
    this.callnumber.callNumber(fono ,true)
    .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));
  }

  public sendEmail(correo:string){
    this.socialSharing.shareViaEmail("","Contacto",[correo],null,null,null)
    .then(res => console.log('',res))
    .catch(err => console.log('', err));
  }

}

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailAfiliadorPage } from './detail-afiliador.page';

describe('DetailAfiliadorPage', () => {
  let component: DetailAfiliadorPage;
  let fixture: ComponentFixture<DetailAfiliadorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailAfiliadorPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailAfiliadorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

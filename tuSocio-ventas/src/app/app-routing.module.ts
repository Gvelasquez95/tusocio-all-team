import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'splash', pathMatch: 'full' },
  { path: 'splash', loadChildren: './splash/splash.module#SplashPageModule' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'registro', loadChildren: './registro/registro.module#RegistroPageModule' },
  { path: 'recoverpass', loadChildren: './recoverpass/recoverpass.module#RecoverpassPageModule' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'perfil', loadChildren: './perfil/perfil.module#PerfilPageModule' },
  { path: 'perfil/detalle', loadChildren: './perfil/detalle/detalle.module#DetallePageModule' },
  { path: 'producto', loadChildren: './producto/producto.module#ProductoPageModule' },
  { path: 'producto/detalle', loadChildren: './producto/detalle/detalle.module#DetallePageModule' },
  { path: 'interesados', loadChildren: './interesados/interesados.module#InteresadosPageModule' },
  { path: 'detalle/:idInteresado', loadChildren: './interesados/detalle/detalle.module#DetallePageModule' },
  { path: 'ventas', loadChildren: './ventas/ventas.module#VentasPageModule' },
  { path: 'detalle/:idVentas', loadChildren: './ventas/detalle/detalle.module#DetallePageModule' },
  { path: 'empresa', loadChildren: './empresa/empresa.module#EmpresaPageModule' },
  { path: 'empresa/detalle', loadChildren: './empresa/detalle/detalle.module#DetallePageModule' },
  { path: 'empresa/register', loadChildren: './empresa/register/register.module#RegisterPageModule' },
  { path: 'add-afiliador', loadChildren: './add-afiliador/add-afiliador.module#AddAfiliadorPageModule' },
  { path: 'detail-afiliador', loadChildren: './detail-afiliador/detail-afiliador.module#DetailAfiliadorPageModule' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-recoverpass',
  templateUrl: './recoverpass.page.html',
  styleUrls: ['./recoverpass.page.scss'],
})
export class RecoverpassPage implements OnInit {

  constructor(public navCtrl:NavController) { }

  ngOnInit() {
  }

  public goPage(pagina){
    this.navCtrl.navigateRoot(pagina);
  }

}

import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NavController, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.page.html',
  styleUrls: ['./producto.page.scss'],
})
export class ProductoPage implements OnInit {

  public listaPlan:any

  constructor(public http:HttpClient, public router:Router, public toastCtrl:ToastController) { }

  ngOnInit() {
    this.getListaOfertas();
  }

  public getListaOfertas(){
    // this.http.get("https://cyhomework.com/app_socio/app_1/plan.php").subscribe(data => {
    //   this.listaPlan = data;
    //   console.log(this.listaPlan);
    // });

        this.listaPlan = [
          {
            idPlan:'2',
            plan:'1',
            descripcion:'BÁSICO',
            foto:'../../assets/images/basico.jpg',
          },
          {
            idPlan:'3',
            plan:'2',
            descripcion:'PREMIUM',
            foto:'../../assets/images/premium.jpg',
          },
          {
            idPlan:'4',
            plan:'3',
            descripcion:'VIP',
            foto:'../../assets/images/vip.jpg',
          }
        ]
        console.log(this.listaPlan);

  }

  public irDetalle(idplan:string,cod:string,descripcion:string){
    if(idplan == '4'){
      this.makeText("¡Proximamente plan para multicadenas!")
    }else{
    this.router.navigate(['/producto/detalle', {idPlan:idplan,cod_plan:cod, plan:descripcion}]);
    }
  }

  async makeText(mensaje:string){
    const toast = await this.toastCtrl.create(
      {
        message: mensaje,
        duration: 2500
      }
    );
    await toast.present();
  }


}

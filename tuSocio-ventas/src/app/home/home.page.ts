import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ActionSheetController, AlertController, ToastController, LoadingController, AngularDelegate } from '@ionic/angular';
import { HttpClient } from "@angular/common/http";
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{

  public idVendedor:string;

  public passAlert:any;

  public clave:string;
  public nclave:string;
  public cnclave:string;

  constructor(public router: Router, public activatedRoute:ActivatedRoute,public actionSheetController: ActionSheetController,
    private socialSharing: SocialSharing, private storage: Storage,  public alertCtrl: AlertController, public toastCtrl: ToastController,
    public loadingCtrl: LoadingController, public http: HttpClient){

  }

  ngOnInit(){
    this.storage.get('idVendedor').then((val) => {
      this.idVendedor = val;
      console.log("idVendedor: "+ this.idVendedor);
    });
  }

  public goPage(pagina){
    this.router.navigate([pagina]);
  }

  async openOptions(){
    const actionSheet = await this.actionSheetController.create({
      header: 'Opciones',
      buttons: [
      {
        text: 'Compatir',
        icon: 'share',
        handler: () => {
          this.shareIt();
        }
      }, {
        text: 'Cambiar contraseña',
        icon: 'key',
        handler: () => {
          this.changePass();
        }
      },{
        text: 'Cerrar Sesión',
        role: 'destructive',
        icon: 'log-out',
        handler: () => {
          this.logOut();
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  public shareIt(){
    this.socialSharing.share("Descubre un mundo lleno de oportunidades, por que somos tu Socio en los Negocios :)", null, null,"https://www.facebook.com/Tusocioapp/?epa=SEARCH_BOX")
    .then(res => console.log('',res))
    .catch(err => console.log('', err));
  }

  async logOut(){
    const logoutalert = await this.alertCtrl.create(
      {
        header: "Cerrar Sesión",
        message: "¿Esta seguro que desea cerrar sesión?",
        buttons:[
          {
            text: 'Cancelar',
            role: 'cancel',
            handler: () => {
              logoutalert.dismiss();
              return false;
            }
          },
          {
            text: 'Salir',
            handler: () => {
              this.storage.remove('idVendedor');
              this.storage.remove('cargoVendedor');
              this.router.navigate(['/login']);
            }
          }
        ]
      });

      await logoutalert.present();
  }

  async changePass(){
    this.passAlert = await this.alertCtrl.create ({
        header:"Cambiar contraseña",
        inputs: [
          {
            name: 'clave',
            type: 'password',
            placeholder: 'Ingrese contraseña'
          },
          {
            name: 'nclave',
            type: 'password',
            placeholder: 'Ingrese nueva contraseña'  
          },
          {
            name: 'cnclave',
            type: 'password',
            placeholder: 'Confirme nueva contraseña'
          }
        ],
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            handler: () => {
              this.passAlert.dismiss();
              return false;
            }          
          },{
            text: 'Cambiar',
            handler: data => {
              this.clave = data.clave;
              this.nclave = data.nclave;
              this.cnclave = data.cnclave;
              this.changerUrPass();
              return false;
            }          
          }
        ]   
      });

      await this.passAlert.present();
  }

  async changerUrPass(){
    var url="http://mundots.com/app_socio/app_2/perfil.php?change";

    var fp = new FormData();
    fp.append('user', this.idVendedor);
    fp.append('clave', this.clave);
    fp.append('clave_n', this.nclave);

    const loading = await this.loadingCtrl.create(
      {
        message: "Verificando Datos...",
        duration: 1000
      }
    );
    await loading.present();

      if(this.nclave == this.cnclave){
          this.http.post(url, fp).subscribe(data => {
          let result = JSON.parse(JSON.stringify(data || null ));
          let mensaje=result.status;

            if(mensaje == "success"){
              this.makeText("!Enhorabuena! Tu contraseña ha sido actualizada.");
              this.passAlert.dismiss();
            }
            else{
              this.makeText(mensaje);
            }
              this.loadingCtrl.dismiss();
            },err => {
              this.loadingCtrl.dismiss();
            console.log(err);
            });
      }else{
        this.makeText("Los campos de nueva contraseña no coinciden, verificalo porfavor.");
      }

  }

  async makeText(mensaje:string){
    const toast = await this.toastCtrl.create(
      {
        message: mensaje,
        duration: 2500
      }
    );
    await toast.present();
  }


}

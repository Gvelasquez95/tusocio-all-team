import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {

  constructor(public navCtrl: NavController) { }

  ngOnInit() {
  }

  public goBack(){
    this.navCtrl.navigateBack('/login');
  }

}

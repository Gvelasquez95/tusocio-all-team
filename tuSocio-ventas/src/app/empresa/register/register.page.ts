import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { LoadingController, ToastController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  public idVendedor:string;

  //Parametros
  public nombre_socio:string;
  public tdoc_socio:string;
  public ndoc_socio:string;
  public razon:string;
  public ruc:string;
  public correo:string;
  public telefono:string;
  public direccion:string;
  public empresa:string;

  public isToggled: boolean;
  showCard;

  constructor(public http: HttpClient, private storage: Storage, public loadingCtrl: LoadingController,
    public toastCtrl: ToastController, public navCtrl: NavController) { 

      this.isToggled = false;
      this.empresa = "0";
  }

  notify() {
    this.showCard = !this.showCard;

    if(this.isToggled == true){
      this.empresa = "1";
    }else if(this.isToggled == false){
      this.empresa = "0";
    }
    console.log(this.empresa);
    
  }

  ngOnInit() {
    this.storage.get('idVendedor').then(val => {
      this.idVendedor = val;
      console.log(this.idVendedor);
    })
  }

  async addSocio(){
    var url ="http://mundots.com/app_socio/app_2/registro_socio.php?user="+this.idVendedor;

    var fs = new FormData();
    fs.append('user', this.idVendedor);
    fs.append('nombre', this.nombre_socio);
    fs.append('tipo_doc', this.tdoc_socio);
    fs.append('nro_doc', this.ndoc_socio);
    fs.append('razon_s', this.razon);
    fs.append('ruc', this.ruc);
    fs.append('correo', this.correo);
    fs.append('telefono', this.telefono);
    fs.append('direccion', this.direccion);
    fs.append('empresa', this.empresa);

    const loading = await this.loadingCtrl.create(
      {
        message: "Realizando registro...",
        duration: 1500
      }
    );
    await loading.present();

    this.http.post(url, fs).subscribe(data => {
    let result = JSON.parse(JSON.stringify(data || null ));
    let mensaje=result.status;

     if(mensaje == "success"){
      this.makeText("!Excelente! Registraste a un nuevo socio ;)");
      this.navCtrl.navigateBack('/home');
    }
     else{
      this.makeText(mensaje);
    }
      this.loadingCtrl.dismiss();
    },err => {
    this.loadingCtrl.dismiss();
    console.log(err);
   });

  }

  async makeText(mensaje:string){
    const toast = await this.toastCtrl.create(
      {
        message: mensaje,
        duration: 2500
      }
    );
    await toast.present();
  }

}

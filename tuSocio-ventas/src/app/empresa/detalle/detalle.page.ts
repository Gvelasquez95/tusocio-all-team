import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { CallNumber } from '@ionic-native/call-number/ngx';
import { ActivatedRoute } from '@angular/router';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.page.html',
  styleUrls: ['./detalle.page.scss'],
})
export class DetallePage implements OnInit {

  public idSocio:string;
  public bussinessData:any;

  public cargoVendedor:string;

  public editEnable:boolean;
  read=null;

  //variables
  public nombre_socio:any;
  public tdoc_socio:any;
  public ndoc_socio:any;

  public logo_socio:any;
  public razon:string;
  public ruc:string;
  public correo:string;
  public telefono:string;
  public direccion:string;

  constructor(public http:HttpClient,private callNumber: CallNumber, public activatedRoute: ActivatedRoute, private storage: Storage) {
    this.editEnable = false;
  }

  ngOnInit() {
    this.storage.get('cargoVendedor').then( val => {
      this.cargoVendedor = val;
      console.log("cargo vendedor: "+this.cargoVendedor);
    });

    this.idSocio = this.activatedRoute.snapshot.paramMap.get('idSocio');
    this.getBussinessData(this.idSocio);
  }

  public makEditable(){
    this.editEnable = !this.editEnable;
    if (this.read==1) {
      this.read=null;
    }
    else if (this.read==null) {
      this.read=1;
    }
  }

  public getBussinessData(idSocio){
    this.http.get("http://mundots.com/app_socio/app_2/lista_socio_id.php?idSocio="+idSocio).subscribe(data => {
      this.bussinessData = data;
      console.log(this.bussinessData);
      
        for( let sdata of this.bussinessData){
          this.nombre_socio = sdata.nombre;
          this.tdoc_socio = sdata.tipo_doc;
          this.ndoc_socio = sdata.nro_doc;

          if(sdata.logo == ''){
            this.logo_socio = "../../assets/images/logo.png";
          }else{
            this.logo_socio = sdata.logo;
          }

            this.razon = sdata.razon_s;
            this.ruc = sdata.ruc;
            this.telefono = sdata.telefono;
            this.correo = sdata.correo;
            this.direccion = sdata.direccion;
        }
      });
  }

  public callSocio(bussinessNumber:string){
    this.callNumber.callNumber(bussinessNumber, true)
    .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));
  }

}

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastController, NavController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-empresa',
  templateUrl: './empresa.page.html',
  styleUrls: ['./empresa.page.scss'],
})
export class EmpresaPage implements OnInit {

  public idVendedor:string;
  public ListaEmpresaFree:any=[]
  public ListaEmpresaPaga:any=[]
  public listafiltrofree:any=[];
  public listafiltropago:any=[];
  public opciones='free';

  public searchQuery:string;

  constructor(public router: Router, public activatedRoute: ActivatedRoute,public http: HttpClient,
     public toastCtrl:ToastController, public navCtrl: NavController, public storage: Storage) { 
  }

  irDetalle(idSocio:string){
    this.router.navigate(['/empresa/detalle', {idSocio:idSocio} ]);
  }


  ngOnInit() {
   
    this.storage.get('idVendedor').then(val => {
      this.idVendedor = val;
      console.log(this.idVendedor);
      this.getSociosFree(this.idVendedor);
      this.getSocioPlan(this.idVendedor);
    })
  }

  public getSociosFree(id:string){
    this.http.get("http://mundots.com/app_socio/app_2/lista_socios_u.php?free&user="+id).subscribe(data => {
      this.ListaEmpresaFree = data;
      this.listafiltrofree=data;
    });
  }

  public getSocioPlan(id:string){
    this.http.get("http://mundots.com/app_socio/app_2/lista_socios_u.php?plan&user="+id).subscribe(data => {
      this.ListaEmpresaPaga = data;
      this.listafiltropago=data;
    });
  }

  public addSocio(){
    this.router.navigate(['/empresa/register']);
  } 

  segmentChanged(ev:any){
    this.opciones = ev.detail.value;
    console.log(ev);
    
    this.searchList();
  }

  searchList(): void {

    
    const query = (this.searchQuery && this.searchQuery !== null) ? this.searchQuery : '';

    if (this.opciones === 'free') {
      this.ListaEmpresaFree=this.listafiltrofree;
      this.ListaEmpresaFree = this.filterList(this.ListaEmpresaFree, query);
    } else if (this.opciones === 'paga') {
      this.ListaEmpresaPaga=this.listafiltropago;
      this.ListaEmpresaPaga = this.filterList(this.ListaEmpresaPaga, query);
    }
  }

  filterList(list, query): Array<any> {
    return list.filter(item => item.razon_s.toLocaleLowerCase().includes(query.toLocaleLowerCase())); 
  }

}

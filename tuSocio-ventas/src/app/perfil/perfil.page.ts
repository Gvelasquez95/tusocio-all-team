import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Storage } from '@ionic/storage';
import { HttpClient } from "@angular/common/http";
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit {

  public vendedorData:any;
  public liderData:any;
  public equipoData:any;
  public idVendedor:string;

  //Parametros Vendedor
  public foto_usuario:string;
  public nombre:string;
  public cargo:String;
  public puesto:string;
  public fecha:string;
  public afiliaciones:string;
  public equipo:string;
  public habilidades:string;
  public referido:string;
  public tipo:string;
  public ListaEquipo:any;

  //Parametros lider
  public foto_lider:string;
  public nombre_lider:string;
  public puesto_lider:string; 
  public cargo_lider:string;
  public numero_lider:string;

  //Parametros equipo
  public id_equipo:string;
  public foto_equipo:string;
  public nombre_equipo:string;

  constructor(public router: Router, public activatedRoute: ActivatedRoute,private callNumber: CallNumber,
    private socialSharing: SocialSharing, public storage: Storage, public http:HttpClient, public navCtrl: NavController) { 
    
  }

  ngOnInit(){
    this.storage.get('idVendedor').then((val) => {
      this.idVendedor = val;
      console.log("vendedor: "+this.idVendedor);
      this.getDataVendedor(this.idVendedor); 
    });
  }

  public getDataVendedor(id:string){
    this.http.get("http://mundots.com/app_socio/app_2/lista_user.php?perfil&user="+id).subscribe(data => {
      this.vendedorData = data;

        for( let vdata of this.vendedorData){
          if(vdata.foto == ''){
            this.foto_usuario = "../../assets/images/logo.png";
          }else{
            this.foto_usuario = vdata.foto;
          }
          
          this.nombre = vdata.nombre;
          if(vdata.habilidades==''){
            this.habilidades = "Ninguna";
          }else{
            this.habilidades = vdata.habilidades;
          }
         
          this.tipo = vdata.tipo;
          this.fecha = vdata.fecha;
          this.afiliaciones = vdata.afiliaciones;
          this.equipo = vdata.equipo;
          this.referido = vdata.referido;
          console.log("referido: "+this.referido);
          

          if(vdata.tipo == "1"){
            this.puesto = "Lider Franquiciado";
            this.cargo = "Lider";
            this.getDataEquipo(this.idVendedor);
          }else if(vdata.tipo == "2"){
            this.puesto = "Empresario Independiente";
            this.cargo = "Afiliador";
            this.getDataLider(this.referido);
          }else if(vdata.tipo == "3"){
            this.puesto = "Administrador";
            this.cargo = "Administrador";
            this.getDataLideres();
           }
          }
        });
  }

  public getDataLider(id:string){
    this.http.get("http://mundots.com/app_socio/app_2/lista_user.php?perfil&user="+id).subscribe(data => {
      this.liderData = data;
        for( let ldata of this.liderData){

            if(ldata.foto == ''){
              this.foto_lider = "../../assets/images/logo.png";
            }else{
              this.foto_lider = ldata.foto;
            }

            this.nombre_lider = ldata.nombre;
            this.numero_lider = ldata.telefono;

            if(ldata.tipo == "1"){
              this.puesto_lider = "Lider Franquiciado";
              this.cargo_lider = "Lider";
            }
        }
    });
  }

  public getDataEquipo(id:string){
    this.http.get("http://mundots.com/app_socio/app_2/lista_user.php?equipo&user="+id).subscribe(data => {
      this.equipoData = data;
    });
  }

  public getDataLideres(){
  this.http.get("http://mundots.com/app_socio/app_2/lider.php?lista").subscribe(data => {
      this.equipoData = data;
      console.log(this.equipoData);  
    });
  }

  public detalleMiembroEquipo(idMiembro){
    this.router.navigate(['/detail-afiliador',{idMiembro:idMiembro}]);
  }


  public makeCall(numero:string){
    this.callNumber.callNumber(numero, true)
    .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));
  }

  public sendMessage(numero:string){
    this.socialSharing.shareViaSMS("",numero)
    .then(res => console.log('',res))
    .catch(err => console.log('', err));
  }

  public addAfiliador(tipo:string){
    this.router.navigate(['/add-afiliador',{cod_tipo: tipo}]);
  }

  public detalleAfiliador(){
    this.navCtrl.navigateForward('/detail-afiliador');
  }

  public goPage(pagina,id:string){
    this.router.navigate([pagina, {idSocio:id}]);
  }

}

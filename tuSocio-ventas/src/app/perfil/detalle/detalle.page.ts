import { Component, OnInit } from '@angular/core';
import { NavController, ToastController, LoadingController, ActionSheetController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Camera } from '@ionic-native/camera/ngx';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.page.html',
  styleUrls: ['./detalle.page.scss'],
})
export class DetallePage implements OnInit {

  public vendedorData:any;

  public idVendedor:string;
  public fotoVendedor:string;
  public nombre:string;
  public tipo_doc:string;
  public nro_doc:string;
  public sexo:string;
  public fecha_nac:string;
  public telefono:string;
  public correo:String;
  public habilidades:string;


  constructor(public navCtrl: NavController, public http: HttpClient, public toastCtrl: ToastController, private camera: Camera,
    public transfer: FileTransfer, public router: Router, private storage: Storage, public loadingCtrl: LoadingController,
    public actionSheetCtrl: ActionSheetController) { }

  ngOnInit() {
    this.storage.get('idVendedor').then(val => {
      this.idVendedor = val;
      console.log("Detalle Perfil: "+this.idVendedor);
      this.getDataVendedor(this.idVendedor);
    })
  }

  public getDataVendedor(id:string){
    this.http.get("https://www.cyhomework.com/app_socio/app_2/lista_user.php?perfil&user="+id).subscribe(data => {
      this.vendedorData = data;

        for( let vdata of this.vendedorData){
          if(vdata.foto == ''){
            this.fotoVendedor = "../../assets/images/logo_bg_blanco.png";
          }else{
            this.fotoVendedor = vdata.foto;
          }
          
          this.nombre = vdata.nombre;
          this.tipo_doc = vdata.tipo_doc;
          this.nro_doc = vdata.nro_doc;
          this.sexo = vdata.sexo;
          this.fecha_nac = vdata.fecha_nac;
          this.telefono = vdata.telefono;
          this.correo = vdata.correo;

          if(vdata.habilidades==''){
            this.habilidades = "Ninguna";
          }else{
            this.habilidades = vdata.habilidades;
          }
         
          }
        });
  }

  async updateVendedor(){

    if(this.fotoVendedor.includes('https://www.cyhomework.com/app_socio/app_2/user_pf')){
      
      var url="https://www.cyhomework.com/app_socio/app_2/perfil.php?perfil";
        
      var fp = new FormData();
      fp.append('user', this.idVendedor);
      fp.append('nombre', this.nombre);
      fp.append('tipo_doc', this.tipo_doc);
      fp.append('nro_doc', this.nro_doc);
      fp.append('telefono', this.telefono);
      fp.append('sexo', this.sexo);
      
      const loading = await this.loadingCtrl.create(
        {
          message: "Estamos actualizando su perfil..."
        }
      );
      await loading.present();
  
      this.http.post(url, fp).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data || null ));
      let mensaje=result.status;
  
      if(mensaje == "success"){
        this.makeText("¡Bien! tu información ha sido actualizada.");
        loading.dismiss();
        this.router.navigate(['/home']);
      }
      else{
        this.makeText(mensaje);
      }
        this.loadingCtrl.dismiss();
      },err => {
        this.loadingCtrl.dismiss();
      console.log(err);
    });

    }else{

    var url="https://www.cyhomework.com/app_socio/app_2/perfil.php?perfil";

    var targetPath = this.fotoVendedor;

    var options = {
      fileKey: "foto",
      chunkedMode: false,
      mimeType: "multipart/form-data",
      params : {'user': this.idVendedor,
                'nombre': this.nombre,
                'tipo_doc': this.tipo_doc,
                'nro_doc': this.nro_doc,
                'telefono': this.telefono,
                'sexo': this.sexo},
      HttphttpMethod: 'POST'
    };

    const fileTransfer: FileTransferObject = this.transfer.create();

    const loading = await this.loadingCtrl.create(
      {
        message: "Estamos actualizando su perfil..."
      }
    );

    await loading.present();

    // Use the FileTransfer to upload the image
    fileTransfer.upload(targetPath, url, options).then(data => {
      if(data.response == 'success'){
        this.makeText("¡Bien! tu información ha sido actualizada.");
        loading.dismiss();
        this.router.navigate(['/home']);
      }else{
        loading.dismiss()
        this.makeText(data.response);
      }
    }, err => {
      loading.dismiss()
      this.makeText('Ocurrio un Error mientras se actualizaba tu perfil.');
    });

  }

}

  async seleccione() {
    const actionSheet = await this.actionSheetCtrl.create({
      header: 'Elija una opción',
      buttons: [
        {
          text: 'Galeria de Fotos',
          icon: 'image',
          handler: () => {
            this.openGallery();
          }
        },
        {
          text: 'Tomar Foto',
          icon: 'camera',
          handler: () => {
            this.takePhoto();
          }
        },
        {
          text: 'Cancelar',
          role: 'destructive',
          handler:()=>{
            actionSheet.dismiss();
          }
        }
      ]
    });
    await actionSheet.present();
  }

  public takePhoto() {
    this.camera.getPicture({
      quality: 100,
      sourceType: this.camera.PictureSourceType.CAMERA,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      allowEdit:true,
      saveToPhotoAlbum: true,
    }).then((imageData) => {
      this.fotoVendedor = "data:image/jpeg;base64," + imageData;
     }, (err) => {
      console.log(err);
    });
  }

  public openGallery(){
    this.camera.getPicture({
      quality: 100,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      allowEdit:true
    }).then((imageData) => {
      this.fotoVendedor = "data:image/jpeg;base64," + imageData;
     }, (err) => {
      console.log(err);
    });
  }

  async makeText(message){
    const toast = await this.toastCtrl.create({
      message: message,
      duration: 2500
    });
    await toast.present();
  }

}
